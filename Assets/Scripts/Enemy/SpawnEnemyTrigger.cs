using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemyTrigger : MonoBehaviour
{
    public GameObject enemyPrefab;

    public Transform spawnLocation;

    public void SpawnTheEnemy()
    {
        Instantiate(enemyPrefab, spawnLocation.position, spawnLocation.localRotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SpawnTheEnemy();
            gameObject.SetActive(false);
        }
    }

    public void SpawnByEvent()
    {
        SpawnTheEnemy();
        gameObject.SetActive(false);
    }
}
