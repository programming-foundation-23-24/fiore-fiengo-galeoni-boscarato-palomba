using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody rb;

    [HideInInspector]
    public MeshRenderer enemyBody;
    public GameObject R_Arm;
    [HideInInspector]
    public MeshRenderer R_ArmMesh;
    public GameObject L_Arm;
    [HideInInspector]
    public MeshRenderer L_ArmMesh;

    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
        enemyBody = GetComponent<MeshRenderer>();
        L_ArmMesh = L_Arm.GetComponent<MeshRenderer>();
        R_ArmMesh = R_Arm.GetComponent<MeshRenderer>();
    }
}
