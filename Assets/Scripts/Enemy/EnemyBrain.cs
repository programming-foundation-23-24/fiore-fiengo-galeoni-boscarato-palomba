using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBrain : MonoBehaviour
{
    [SerializeField] GoTowardsPlayer movement;
    [SerializeField] EnemyHealth health;

    private void Awake()
    {
        movement = GetComponent<GoTowardsPlayer>();
        health = GetComponent<EnemyHealth>();
    }

    public void ValuesAllocation(float myspeed, int myhealth)
    {
        movement.speed = myspeed;
        health.maxHealth = myhealth;
    }
}
