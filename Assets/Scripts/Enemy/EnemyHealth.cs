using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemyHealth : BaseEnemy
{
    public int currentHealth;
    public int maxHealth;

    public float knockbackForce = 100;
    GoTowardsPlayer goTowardsPlayer;

    protected override void Start()
    {
        base.Start();
        currentHealth = maxHealth;

        goTowardsPlayer = GetComponent<GoTowardsPlayer>();
    }

    public void TakeDamage()
    {
        currentHealth -= 1;

        goTowardsPlayer.GotHit();

        CheckIfDead();
    }

    public void CheckIfDead()
    {
        if (currentHealth == 0)
        {
            gameObject.SetActive(false);
            return;
        }

        StartCoroutine(DamageFeedBack());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            TakeDamage();
        }
    }

    IEnumerator DamageFeedBack()
    {
        enemyBody.enabled = false;
        L_Arm.SetActive(false);
        R_Arm.SetActive(false);

        yield return new WaitForSeconds(0.1f);

        enemyBody.enabled = true;
        L_Arm.SetActive(true);
        R_Arm.SetActive(true);
    }
}