using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerToggleByMessage : MonoBehaviour
{
    Collider myCollider;

    private void Awake()
    {
        myCollider = GetComponent<Collider>();
    }

    public void Message()
    {
        StartCoroutine(Anim());
    }

    IEnumerator Anim()
    {
        myCollider.enabled = false;
        yield return new WaitForSeconds(1.5f);
        myCollider.enabled = true;
    }
}
