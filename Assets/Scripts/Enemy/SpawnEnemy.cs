using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    public GameObject enemyPrefab;

    public Transform spawnLocation;

    public float customSpeed;
    public int customMaxHealth;

    [HideInInspector]
    public GameObject enemySpawned;

    public void SpawnTheEnemy()
    {
        GameObject enemy = Instantiate(enemyPrefab, spawnLocation.position, spawnLocation.localRotation);

        enemySpawned = enemy;

        EnemyBrain brain = enemySpawned.GetComponent<EnemyBrain>();

        brain.ValuesAllocation(customSpeed,customMaxHealth);
    }

    public void DeleteEnemy()
    {
        if (enemySpawned != null)
        {
            Destroy(enemySpawned);
        }
    }
}