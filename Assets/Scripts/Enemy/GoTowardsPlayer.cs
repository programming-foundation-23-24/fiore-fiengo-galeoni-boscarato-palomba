using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoTowardsPlayer : BaseEnemy
{
    Transform playerTransform;
    
    public float speed = 5f;

    public bool attacked;
    public float attackCooldown;

    public bool gotHit;

    public Material attackMaterial;
    public Material idleMaterial;
    protected override void Start()
    {
        base.Start();
        GetAllReferences();
    }
    void GetAllReferences()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (!attacked && !gotHit)
        {
            transform.LookAt(playerTransform);

            rb.velocity = transform.forward * speed;

            AttackFeedBack();
        }
        else
        {
            IdleFeedBack();
        }
    }
    void AttackFeedBack()
    {
        enemyBody.material = attackMaterial;
        R_ArmMesh.material = attackMaterial;
        L_ArmMesh.material = attackMaterial;
    }
    void IdleFeedBack()
    {
        enemyBody.material = idleMaterial;
        L_ArmMesh.material = idleMaterial;
        R_ArmMesh.material = idleMaterial;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            attacked = true;
            rb.constraints = RigidbodyConstraints.FreezeAll;
            StartCoroutine(WaitForChase());
        }
    }

    public void GotHit()
    {
        gotHit = true;

        StartCoroutine(JustGotHitSoWait());
    }

    IEnumerator JustGotHitSoWait()
    {
        yield return new WaitForSeconds(0.5f);

        gotHit = false;
    }

    IEnumerator WaitForChase()
    {
        yield return new WaitForSeconds(attackCooldown);
        attacked = false;
        rb.constraints &= (RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ);
    }
}

