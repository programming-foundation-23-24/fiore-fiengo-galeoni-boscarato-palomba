﻿using UnityEngine;

public abstract class BaseProjectile : MonoBehaviour
{
    public string type;
    public float damage;
    public float speed;
    public Rigidbody rb;

    private void Start()
    {
        Destroy(gameObject, 15);
    }

    private void FixedUpdate()
    {
        BulletTraveling();
    }
    public void BulletTraveling()
    {
        rb.velocity = transform.up * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("TriggerDimension") || other.CompareTag("Interactable") || other.CompareTag("IgnoreBullet") || other.CompareTag("Confiner"))
        {
            return;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
