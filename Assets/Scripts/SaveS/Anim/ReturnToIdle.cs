using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToIdle : MonoBehaviour
{
    Animator animator;

    private void Awake()
    {
        animator = GetComponentInParent<Animator>();
    }

    public void IdleMustReturn()
    {
        animator.SetTrigger("Idle");
    }
}
