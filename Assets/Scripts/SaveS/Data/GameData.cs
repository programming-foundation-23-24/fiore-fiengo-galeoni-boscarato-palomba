using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public Vector3 playerPosition;
    public SerializableDictionary<string, BaseDialogueMemory> dialogMemory;
    public SerializableDictionary<string, BaseNoteMemory> noteMemory;
    public List<NoteInteraction> noteInteractions;
    public List<GameObject> dialogueInteractions;
    public List<PuzzleIsSolved> puzzlesSolved;
    public List<int> tastierinoSolution;

    // the values defined in this constructor will be the default values
    // the game starts with when there's no data to load
    public GameData() 
    {
        playerPosition = new Vector3(2, 1, 7);
        dialogMemory = new SerializableDictionary<string, BaseDialogueMemory>();
        noteMemory = new SerializableDictionary<string, BaseNoteMemory>();
        noteInteractions = new List<NoteInteraction>();
        dialogueInteractions = new List<GameObject>();
        puzzlesSolved = new List<PuzzleIsSolved>();
        tastierinoSolution = new List<int>();
    }
}
