using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PuzzleSolution : MonoBehaviour
{
    public UnityEvent OnPuzzleSolved;

    public AudioSource completionSound;

    public bool playSoundOnSolved = true;

    public void PuzzleSolved()
    {
        GameManager.Instance.PuzzleIsNotOnScreen();
        GameManager.Instance.StartPlayer();

        if (playSoundOnSolved)
        {
            completionSound.Play();
        }

        OnPuzzleSolved.Invoke();
    }
}
