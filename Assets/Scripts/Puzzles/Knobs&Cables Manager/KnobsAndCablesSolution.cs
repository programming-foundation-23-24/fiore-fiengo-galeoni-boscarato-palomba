using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnobsAndCablesSolution : PuzzleSolution
{

    public bool isPresent;

    [Header("Knobs")]
    public TotalEnergyKnob totalEnergyKnob;
    public int presentEnergyKnobSolution;
    public int futureEnergyKnobSolution;

    [Header("Cables")]
    public KnobCheck totalEnergyCables;
    public float presentEnergyCablesSolution;
    public float futureEnergyCablesSolution;

    public void CheckForSolution()
    {
        /*if (isPresent)
        {
            if (totalEnergyKnob.totEnergy == presentEnergyKnobSolution && totalEnergyCables.currentPower == presentEnergyCablesSolution)
            {
                PuzzleSolved();
            }
        }
        else
        {
            if (totalEnergyKnob.totEnergy == futureEnergyKnobSolution && totalEnergyCables.currentPower == futureEnergyCablesSolution)
            {
                PuzzleSolved();
            }
        }*/
    }
}
