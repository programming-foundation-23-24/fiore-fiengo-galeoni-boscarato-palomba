using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeCFuture : PuzzleSolution
{
    [Header("Cables")]
    public KnobCheck cablesBrain;

    public bool cableSolved;

    public void CheckForSolution()
    {
        if (CableSolved())
        {
            PuzzleSolved();
        }
    }
    bool CableSolved()
    {
        return cableSolved;
    }
}
