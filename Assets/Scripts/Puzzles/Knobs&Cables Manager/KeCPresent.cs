using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class KeCPresent : PuzzleSolution
{
    [Header("Knobs")]
    public TotalEnergyKnob knobsBrain;

    public int presentEnergyKnobSolution;

    [Header("Cables")]
    public ColorCheck cablesBrain;

    public bool cableSolved;

    public void CheckForSolution()
    {
        if (KnobsSolved() && CableSolved())
        {
            PuzzleSolved();
        }
    }
    bool KnobsSolved()
    {
        return knobsBrain.totEnergy == presentEnergyKnobSolution;
    }
    bool CableSolved()
    {
        return cableSolved;
    }
}
