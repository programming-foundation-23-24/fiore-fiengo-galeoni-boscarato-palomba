using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ColorCheck : PuzzleSolution
{
    public List<SocketDataColored> color = new List<SocketDataColored>();

    public GameObject bloccoInteraction;

    [SerializeField] int correctSocketColor1;
    [SerializeField] int correctSocketColor2;
    [SerializeField] int correctSocketColor3;
    int socketColor1;
    int socketColor2;
    int socketColor3;

    public Color orange;
    public Color green;
    public Color purple;

    public Image solutionVisualizer1;
    public Image solutionVisualizer2;
    public Image solutionVisualizer3;

    public KeCPresent KeCPresent;

    bool playSound = false;

    private void Update()
    {
        FeedBackFeeder();

        socketColor1 = CombineColors(color[0].colorInt, color[1].colorInt);
        socketColor2 = CombineColors(color[2].colorInt, color[3].colorInt);
        socketColor3 = CombineColors(color[4].colorInt, color[5].colorInt);

        if (socketColor1 == correctSocketColor1 && socketColor2 == correctSocketColor2 && socketColor3 == correctSocketColor3)
        {
            KeCPresent.cableSolved = true;

            bloccoInteraction.SetActive(true);

            PlaySound();
        }
        else
        {
            playSound = false;

            KeCPresent.cableSolved = false;

            bloccoInteraction.SetActive(false);
        }
    }

    private int CombineColors(int color1, int color2)
    {
        return (color1 + color2);
    }

    void FeedBackFeeder()
    {
        if (socketColor1 == correctSocketColor1)
        {
            solutionVisualizer1.color = orange;
        }
        else
        {
            solutionVisualizer1.color = Color.gray;
        }

        if (socketColor2 == correctSocketColor2)
        {
            solutionVisualizer2.color = green;
        }
        else
        {
            solutionVisualizer2.color = Color.gray;
        }

        if (socketColor3 == correctSocketColor3)
        {
            solutionVisualizer3.color = purple;
        }
        else
        {
            solutionVisualizer3.color = Color.gray;
        }
    }

    public void PlaySound()
    {
        if (!playSound)
        {
            completionSound.Play();

            playSound = true;
        }
    }
}
