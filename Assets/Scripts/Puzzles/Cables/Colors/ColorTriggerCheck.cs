using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorTriggerCheck : MonoBehaviour
{
    public Transform socket;

    public SocketDataColored socketData;

    public bool isTouchingSocket;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        socket = collision.gameObject.transform;
        socketData = collision.GetComponent<SocketDataColored>();

        isTouchingSocket = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        socket = null;
        socketData = null;

        isTouchingSocket = false;
    }
}
