using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocketDataColored : MonoBehaviour
{
    public bool isConnected;

    public KnobCablePower cablePower;

    public int colorInt;

    public string color;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isConnected)
        {
            cablePower = collision.GetComponentInParent<KnobCablePower>();
        }
    }

    public void AddColor()
    {
        colorInt = cablePower.color;
    }
    public void RemoveColor()
    {
        colorInt = 0;
    }
}
