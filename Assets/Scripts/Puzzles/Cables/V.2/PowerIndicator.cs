using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PowerIndicator : MonoBehaviour
{
    public float maxPower;
    public float currentPower;

    [SerializeField] List<SocketData> socketData = new List<SocketData>();
    [SerializeField] Slider powerLevel;
    [SerializeField] TextMeshProUGUI hrzPowerLevel;

    private void Start()
    {
        powerLevel = GetComponentInChildren<Slider>();
        powerLevel.maxValue = maxPower;
    }

    public void CheckConnection()
    {
        foreach (SocketData socket in socketData)
        {
            if (socket.isConnected && !socket.isPowerDelivered)
            {
                powerLevel.value += socket.cablePower.power * socket.multiplier;

                socket.isPowerDelivered = true;

                hrzPowerLevel.text = $"{powerLevel.value} THz";
            }
        }
        CheckCurrentPower();
    }

    public void RemoveConnection()
    {
        foreach (SocketData socket in socketData)
        {
            if (!socket.isConnected && socket.cablePower != null)
            {
                powerLevel.value -= socket.cablePower.power * socket.multiplier;

                socket.isPowerDelivered = false;

                hrzPowerLevel.text = $"{powerLevel.value} THz";
            }
        }
        CheckCurrentPower();
    }

    private void CheckCurrentPower()
    {
        currentPower = powerLevel.value;
    }

    public void Emergency()
    {
        powerLevel.value = 0;
        hrzPowerLevel.text = $"0 THz";
    }
}
