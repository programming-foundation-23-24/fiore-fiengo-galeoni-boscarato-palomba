using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCheck : MonoBehaviour
{
    public Transform socket;
    public SocketData socketData;

    public bool isTouchingSocket;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        socket = collision.gameObject.transform;
        socketData = collision.GetComponent<SocketData>();

        isTouchingSocket = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        socket = null;
        socketData = null;

        isTouchingSocket = false;
    }
}
