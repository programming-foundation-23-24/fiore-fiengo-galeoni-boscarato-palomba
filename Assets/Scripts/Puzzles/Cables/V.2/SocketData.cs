using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocketData : MonoBehaviour
{
    public float multiplier;

    public bool isConnected;

    public bool isPowerDelivered;

    public CablePower cablePower;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        cablePower = collision.GetComponentInParent<CablePower>();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        cablePower = null;
    }
}
