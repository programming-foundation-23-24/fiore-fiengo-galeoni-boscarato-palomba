using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Cables1 : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    public PowerIndicator powerIndicator;

    Transform cableStart;
    Vector2 startPosition = new Vector2();

    TriggerCheck triggerCheck;

    private void Start()
    {
        GetAllReferences();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (triggerCheck.socketData != null)
        {
            triggerCheck.socketData.isConnected = false;
        }
        
        powerIndicator.RemoveConnection();
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;

        Vector2 direction = new Vector2(transform.position.x, transform.position.y) - startPosition;
        transform.right = direction;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (triggerCheck.isTouchingSocket && !triggerCheck.socketData.isConnected)
        {
            transform.position = triggerCheck.socket.position;

            triggerCheck.socketData.isConnected = true;
            
            powerIndicator.CheckConnection();
        }
        else
        {
            transform.position = startPosition;
        }

        transform.right = Vector2.zero;
    }
    void GetAllReferences()
    {
        cableStart = transform.parent;
        startPosition = transform.position;

        triggerCheck = GetComponent<TriggerCheck>();

        powerIndicator = GetComponentInParent<PowerIndicator>();
    }
}