using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnobSocket : MonoBehaviour
{
    public bool isConnected;
    public KnobPower cablePower;
    public int value;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isConnected)
        {
            cablePower = collision.GetComponentInParent<KnobPower>();
        }
    }

    public void AddValue()
    {
        value = cablePower.value;
    }
    public void RemoveValue()
    {
        value = 0;
    }
}
