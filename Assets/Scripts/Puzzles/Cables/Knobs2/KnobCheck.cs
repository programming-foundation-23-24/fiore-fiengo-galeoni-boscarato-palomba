using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnobCheck : PuzzleSolution
{

    public List<KnobSocket> sockets = new List<KnobSocket>();

    [SerializeField] int correctSocketValue1;
    [SerializeField] int correctSocketValue2;
    [SerializeField] int correctSocketValue3;

    public int socketValue1;
    public int socketValue2;
    public int socketValue3;

    public KeCFuture KeCFuture;

    public Image solutionVisualizer1;
    public Image solutionVisualizer2;
    public Image solutionVisualizer3;


    private void Update()
    {
        FeedBackFeeder();

        socketValue1 = CombineValues(sockets[0].value, sockets[1].value);
        socketValue2 = CombineValues(sockets[2].value, sockets[3].value);
        socketValue3 = CombineValues(sockets[4].value, sockets[5].value);

        if (socketValue1 == correctSocketValue1 && socketValue2 == correctSocketValue2 && socketValue3 == correctSocketValue3)
        {
            KeCFuture.cableSolved = true;
        }
        else
        {
            KeCFuture.cableSolved= false;
        }
    }

    private int CombineValues(int value1, int value2)
    {
        return (value1 + value2);
    }

    void FeedBackFeeder()
    {
        if(socketValue1 == correctSocketValue1)
        {
            solutionVisualizer1.color = Color.green;
        }
        else
        {
            solutionVisualizer1.color = Color.red;
        }

        if (socketValue2 == correctSocketValue2)
        {
            solutionVisualizer2.color = Color.green;
        }
        else
        {
            solutionVisualizer2.color = Color.red;
        }

        if (socketValue3 == correctSocketValue3)
        {
            solutionVisualizer3.color = Color.green;
        }
        else
        {
            solutionVisualizer3.color = Color.red;
        }
    }

}
