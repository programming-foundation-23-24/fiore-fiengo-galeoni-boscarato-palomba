using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class KnobUltra : MonoBehaviour, IPointerDownHandler
{
    private Vector3 centerPosition;

    public int maxDegrees;
    public int minDegrees;

    public int degreesOfRotation;

    public List<KnobtriggerCheck> checks = new List<KnobtriggerCheck>();

    List<KnobCables> cables = new List<KnobCables>();

    public AudioSource sourceKnob;
    public AudioSource sourceCable;

    private void Start()
    {
        centerPosition = transform.position;
        transform.eulerAngles = new Vector3(0, 0, minDegrees);

        foreach (var check in checks)
        {
            KnobCables cable = check.GetComponent<KnobCables>();

            cables.Add(cable);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        RotateDunPo();

        ResetMyCables();

        sourceKnob.Play();
        sourceCable.Play();
    }

    void RotateDunPo()
    {
        if (transform.eulerAngles.z >= maxDegrees)
        {
            transform.eulerAngles = new Vector3(0,0,minDegrees);
        }
        else
        {
            transform.Rotate(Vector3.forward, degreesOfRotation);
        }
    }
    void ResetMyCables()
    {
        foreach (var cable in cables)
        {
            cable.gameObject.transform.position = cable.startPosition;
        }
        foreach (var check in checks)
        {
            if (check.socketData != null)
            {
                check.socketData.isConnected = false;
                check.isTouchingSocket = false;
                check.socketData.RemoveValue();
                check.socketData = null;
                check.socket = null;
            }
        }
    }
    private float GetAngle(Vector3 position)
    {
        Vector3 difference = position - centerPosition;
        float angle = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        return angle;
    }
}