using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnobPower : MonoBehaviour
{
    public int value;
    public Knob knob;

    private void Update()
    {
        value = knob.value;
    }

}
