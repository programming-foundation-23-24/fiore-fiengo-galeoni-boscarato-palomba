using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Cables2 : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    [SerializeField] Transform cableStart;
    [SerializeField] Vector2 startPosition = new Vector2();

    [SerializeField] TriggerCheck2 triggerCheck;


    private void Awake()
    {
        GetAllReferences();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (triggerCheck.socketData != null)
        {
            triggerCheck.socketData.isConnected = false;
            triggerCheck.socketData.RemovePower();
        }
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;

        Vector2 direction = new Vector2(transform.position.x, transform.position.y) - startPosition;
        transform.right = direction;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (triggerCheck.isTouchingSocket && !triggerCheck.socketData.isConnected)
        {
            transform.position = triggerCheck.socket.position;
            triggerCheck.socketData.isConnected = true;
            triggerCheck.socketData.AddPower();
        }
        else
        {
            transform.position = startPosition;
            triggerCheck.socketData = null;
        }

        transform.right = Vector2.zero;
    }
    void GetAllReferences()
    {
        cableStart = transform.parent;
        startPosition = transform.position;

        triggerCheck = GetComponent<TriggerCheck2>();

    }
}