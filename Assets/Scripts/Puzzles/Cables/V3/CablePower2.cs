using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CablePower2 : MonoBehaviour 
{
    public float power;

    public TextMeshProUGUI powerText;

    private void Awake()
    {
        powerText.text = power.ToString();
    }
}
