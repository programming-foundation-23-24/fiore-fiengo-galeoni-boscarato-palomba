using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCheck2 : MonoBehaviour
{
    public Transform socket;
    public SocketData2 socketData;

    public bool isTouchingSocket;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        socket = collision.gameObject.transform;
        socketData = collision.GetComponent<SocketData2>();

        isTouchingSocket = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        socket = null;
        socketData = null;

        isTouchingSocket = false;
    }
}
