using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PowerIndicator2 : MonoBehaviour
{
    public float maxPower;
    public float currentPower;

    [SerializeField] Slider powerLevel;
    [SerializeField] TextMeshProUGUI hrzPowerLevel;


    private void Start()
    {
        powerLevel = GetComponentInChildren<Slider>();
        powerLevel.maxValue = maxPower;
    }

    private void Update()
    {
        powerLevel.value = currentPower;
        hrzPowerLevel.text = $"{powerLevel.value} THz";
    }
}
