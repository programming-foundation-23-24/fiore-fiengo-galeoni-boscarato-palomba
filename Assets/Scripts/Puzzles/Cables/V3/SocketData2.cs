using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocketData2 : MonoBehaviour
{
    public float multiplier;
    public bool isConnected;
    [SerializeField] CablePower2 cablePower;
    [SerializeField] PowerIndicator2 indicator;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isConnected)
        {
            cablePower = collision.GetComponentInParent<CablePower2>();
        }
    }
    public void AddPower()
    {
        
       indicator.currentPower += cablePower.power * multiplier;
        
    }

    public void RemovePower()
    {
       indicator.currentPower -= cablePower.power * multiplier;
        
    }


}
