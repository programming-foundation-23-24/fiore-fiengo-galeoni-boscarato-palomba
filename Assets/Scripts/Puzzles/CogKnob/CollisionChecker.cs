using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionChecker : MonoBehaviour
{
    public CogKnobSolution brain;

    [SerializeField] bool isKnob2;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isKnob2)
        {
            brain.knob2InPosition = true;
        }
        else
        {
            brain.knob1InPosition = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (isKnob2)
        {
            brain.knob2InPosition = false;
        }
        else
        {
            brain.knob1InPosition = false;
        }
    }
}
