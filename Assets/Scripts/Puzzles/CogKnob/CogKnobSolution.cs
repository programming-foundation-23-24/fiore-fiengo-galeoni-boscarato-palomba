using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CogKnobSolution : MonoBehaviour
{
    public CogKnobBrain brain;

    public GameObject knob1;
    public GameObject knob2;

    public Image feedbackKnob1;
    public Image feedbackKnob2;

    public bool knob1InPosition;
    public bool knob2InPosition;

    public SelfButtonRotator knob1script;
    public SelfButtonRotator knob2script;

    //public bool puzzleSolved;

    private void Update()
    {
        if (Knob1InPosition())
        {
            feedbackKnob1.color = Color.white;
        }
        else
        {
            feedbackKnob1.color = Color.black;
        }

        if (Knob2InPosition())
        {
            feedbackKnob2.color = Color.white;
        }
        else
        {
            feedbackKnob2.color = Color.black;
        }

        CheckForSolution();
    }

    public void CheckForSolution()
    {
        if (Knob1InPosition() && Knob2InPosition())
        {
            if (!KnobsRotating())
            {
                brain.SolvePuzzle();

                //puzzleSolved = true;
            }
        }
    }

    bool Knob1InPosition()
    {
        return knob1InPosition;
    }
    bool Knob2InPosition()
    {
        return knob2InPosition;
    }
    bool KnobsRotating()
    {
        return knob1script.isRotating || knob2script.isRotating;
    }
}
