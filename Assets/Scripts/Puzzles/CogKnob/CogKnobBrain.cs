using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CogKnobBrain : PuzzleSolution
{
    public CogKnobSolution cogKnobSolution;

    public GameObject myExitButton;

    public List<Button> allButtons = new List<Button>();

    private void Awake()
    {
        foreach (var button in GetComponentsInChildren<Button>())
        {
            allButtons.Add(button);
        }
    }

    public void SolvePuzzle()
    {
        StartCoroutine(ExitAndSolve());
    }

    IEnumerator ExitAndSolve()
    {
        myExitButton.SetActive(false);

        foreach (Button button in allButtons)
        {
            button.enabled = false;
        }

        yield return new WaitForSeconds(1f);

        //Add sound

        PuzzleSolved();
    }
}
