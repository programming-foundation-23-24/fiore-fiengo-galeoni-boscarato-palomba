using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyInitialRotation : MonoBehaviour
{
    [HideInInspector]
    public Quaternion myInitialRotation;

    private void Start()
    {
        myInitialRotation = MyRotation();
    }

    Quaternion MyRotation()
    {
        return transform.rotation;
    }

}
