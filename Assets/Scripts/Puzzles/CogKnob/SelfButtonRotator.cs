using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfButtonRotator : MonoBehaviour
{
    public List<SelfButtonRotator> whoRotatesWithMe = new List<SelfButtonRotator>();

    public List<MyInitialRotation> allInitialRotations = new List<MyInitialRotation>();

    public AudioSource source;

    public float targetZRotation;
    public float rotationTime = 1f;

    public bool reverseRotation;

    [HideInInspector]
    public Quaternion targetRotation;
    public bool isRotating = false;
    private Quaternion initialRotation;

    private void Start()
    {
        initialRotation = InitialRotation();

        targetRotation = TargetRotation(targetZRotation);

        foreach (SelfButtonRotator cogs in whoRotatesWithMe)
        {
            allInitialRotations.Add(cogs.GetComponent<MyInitialRotation>());
        }
    }
    public void RotateFromButton()
    {
        if (!isRotating)
        {
            initialRotation = transform.rotation;

            foreach (MyInitialRotation cogs in allInitialRotations)
            {
                cogs.myInitialRotation = cogs.transform.rotation;
            }

            if (reverseRotation)
            {
                targetRotation = NegativeTargetRotation(targetZRotation);
            }
            else
            {
                targetRotation = TargetRotation(targetZRotation);
            }

            StartCoroutine(RotateCoroutineFromButton());
        }
    }
    private IEnumerator RotateCoroutineFromButton()
    {
        isRotating = true;

        source.Play();

        float elapsedTime = 0f;

        while (elapsedTime < rotationTime)
        {
            // Incrementa il tempo trascorso
            elapsedTime += Time.deltaTime;

            // Calcola il progresso della transizione
            float t = Mathf.Clamp01(elapsedTime / rotationTime);

            transform.rotation = Quaternion.Slerp(initialRotation, targetRotation, t);


            foreach (SelfButtonRotator cogs in whoRotatesWithMe)
            {
                cogs.Rotate(targetZRotation);
            }

            yield return null;
        }

        isRotating = false;
    }

    public void Rotate(float Zrotation)
    {
        if (!isRotating)
        {
            initialRotation = transform.rotation;

            foreach (MyInitialRotation cogs in allInitialRotations)
            {
                cogs.myInitialRotation = cogs.transform.rotation;
            }

            if (reverseRotation)
            {
                targetRotation = NegativeTargetRotation(Zrotation);
            }
            else
            {
                targetRotation = TargetRotation(Zrotation);
            }

            StartCoroutine(RotateCoroutine());
        }
    }

    private IEnumerator RotateCoroutine()
    {
        isRotating = true;

        float elapsedTime = 0f;

        while (elapsedTime < rotationTime)
        {
            // Incrementa il tempo trascorso
            elapsedTime += Time.deltaTime;

            // Calcola il progresso della transizione
            float t = Mathf.Clamp01(elapsedTime / rotationTime);

            transform.rotation = Quaternion.Slerp(initialRotation, targetRotation, t);

            yield return null;
        }

        isRotating = false;
    }

    Quaternion InitialRotation()
    {
        return transform.rotation;
    }
    Quaternion TargetRotation(float Zrotation)
    {
        return transform.rotation * Quaternion.Euler(0f, 0f, -Zrotation);
    }   
    Quaternion NegativeTargetRotation(float Zrotation)
    {
        return transform.rotation * Quaternion.Euler(0f, 0f, Zrotation);
    }
}
