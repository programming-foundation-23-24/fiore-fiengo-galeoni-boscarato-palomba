using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class KnobsOnClick : MonoBehaviour, IPointerDownHandler
{
    private Vector3 centerPosition;

    public int maxDegrees;
    public int minDegrees;
    public int degreesOfRotation;

    public AudioSource source;

    private void Start()
    {
        centerPosition = transform.position;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        RotateDunPo();

        source.Play();
    }

    void RotateDunPo()
    {
        if (transform.eulerAngles.z >= maxDegrees)
        {
            transform.eulerAngles = new Vector3(0, 0, minDegrees);
        }
        else
        {
            transform.Rotate(Vector3.forward, -degreesOfRotation);
        }
    }
    private float GetAngle(Vector3 position)
    {
        Vector3 difference = position - centerPosition;
        float angle = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        return angle;
    }
}
