using UnityEngine;
using UnityEngine.EventSystems;

public class RotatableImage : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    private bool isRotating = false;

    private float startRotation;

    private Vector3 startPosition;
    private Vector3 centerPosition;


    private void Start()
    {
        centerPosition = transform.position;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isRotating = true;
        startPosition = Input.mousePosition;
        startRotation = GetAngle(startPosition);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isRotating)
        {
            float currentRotation = GetAngle(Input.mousePosition);
            float deltaRotation = currentRotation - startRotation;

            transform.Rotate(Vector3.forward, deltaRotation);

            startRotation = currentRotation;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isRotating = false;
    }

    private float GetAngle(Vector3 position)
    {
        Vector3 difference = position - centerPosition;
        float angle = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        return angle;
    }
}