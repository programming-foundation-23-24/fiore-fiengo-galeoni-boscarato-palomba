using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TotalEnergyKnob : MonoBehaviour
{
    public UnityEvent OnPuzzleSolved, OnPuzzleExit;

    public int totEnergy;
    bool playSound = false;

    public Knob knobL;
    public Knob knobR;

    public KeCPresent solutionChecker;

    public Image feedback;

    private void FixedUpdate()
    {
        totEnergy = knobL.value + knobR.value;

        if (totEnergy == solutionChecker.presentEnergyKnobSolution)
        {
            feedback.color = Color.green;

            if (!playSound)
            {
                OnPuzzleSolved.Invoke();
                playSound = true;
            }
        }
        else
        {
            feedback.color = Color.red;
        }
    }

    public void DeactivateTrigger()
    {
        if (totEnergy == solutionChecker.presentEnergyKnobSolution)
        {
            OnPuzzleExit?.Invoke();
        }
    }
}
