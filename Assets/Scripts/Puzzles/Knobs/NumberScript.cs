using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberScript : MonoBehaviour
{
    public int number;

    Knob knob;

    public Image image;

    public Color activeColor;
    public Color deactiveColor;

    private void Start()
    {
        image.color = deactiveColor;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.GetComponent<Knob>().value = number;
        image.color = activeColor;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        collision.gameObject.GetComponent<Knob>().value = 0;
        image.color = deactiveColor;
    }
}
