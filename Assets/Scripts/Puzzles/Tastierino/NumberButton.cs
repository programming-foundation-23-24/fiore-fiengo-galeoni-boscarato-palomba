using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberButton : MonoBehaviour
{
    public int numberPad;

    public TastierinoManager manager;

    public void ButtonPressed()
    {
        manager.AddInputNumber(numberPad);
    }
}
