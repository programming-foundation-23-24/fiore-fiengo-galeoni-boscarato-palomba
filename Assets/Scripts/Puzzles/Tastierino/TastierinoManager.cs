using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TastierinoManager : PuzzleSolution
{
    public TextMeshProUGUI codeText;

    public List<int> inputCode = new List<int>();
    public List<int> solution = new List<int>();
    public List<Button> allButtons = new List<Button>();

    private string inputSequence = "";

    public GameObject myExitButton;

    public void AddSolution(int digit1, int digit2, int digit3, int digit4)
    {
        solution.Add(digit1);
        solution.Add(digit2);
        solution.Add(digit3);
        solution.Add(digit4);
    }

    public void AddInputNumber(int inputNumber)
    {
        inputCode.Add(inputNumber);
        AddNumberOnDisplay(inputNumber.ToString());
        CheckInput();
    }
    void AddNumberOnDisplay(string numero)
    {
        inputSequence += numero;
        DisplayUpdate();
    }

    void DisplayUpdate()
    {
        codeText.text = inputSequence;
    }

    public void CheckInput()
    {
        if (inputCode.Count == 4)
        {
            CheckSolution();
        }
    }
    public void CheckSolution()
    {
        //foreach che guarda entrambe le liste e le compara
        if (solution.SequenceEqual(inputCode))
        {
            Debug.Log("CODICE CORRETTO");
            ClearInput();
            ClearDisplay();
        }
        else
        {
            Debug.Log("CODICE SBAGLIATO COME TE");
            ClearInput();
            ClearDisplayError();
        }
    }
    public void ClearInput()
    {
        inputCode.Clear();
    }
    public void ClearDisplay()
    {
        codeText.text = "ACCESS GRANTED";
        inputSequence = string.Empty;
        myExitButton.SetActive(false);
        StartCoroutine(ResetDisplayWin());
    }
    public void ClearDisplayError()
    {
        codeText.text = "ERROR";
        inputSequence = string.Empty;
        StartCoroutine(ResetDisplay());
    }
    IEnumerator ResetDisplay()
    {
        myExitButton.SetActive(false);
        foreach (Button button in allButtons)
        {
            button.enabled = false;
        }
        yield return new WaitForSeconds(1f);
        codeText.text = "----";
        foreach (Button button in allButtons)
        {
            button.enabled = true;
        }
        myExitButton.SetActive(true);
    } 
    IEnumerator ResetDisplayWin()
    {
        foreach (Button button in allButtons)
        {
            button.enabled = false;
        }
        completionSound.Play();
        yield return new WaitForSeconds(1f);
        codeText.text = "----";
        foreach (Button button in allButtons)
        {
            button.enabled = true;
        }

        PuzzleSolved();
    }

}
