using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ExitPuzzle : MonoBehaviour
{
    public GameObject myPuzzle;

    public BaseInteractable myInteractable;

    public UnityEvent OnClick;

    public bool spaceQuit = false;

    private void Update()
    {
        if (Input.GetButtonDown("Pause") || Input.GetButtonDown("Interact"))
        {
            Quit();
        }
        if(spaceQuit && Input.GetButtonDown("Jump"))
        {
            Quit();
        }
    }

    public void Quit()
    {
        myPuzzle.SetActive(false);

        if (myInteractable != null)
        {
            myInteractable.ShowPrompt();
        }

        GameManager.Instance.StartPlayer();
        GameManager.Instance.PuzzleIsNotOnScreen();

        OnClick?.Invoke();
    }
}
