using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RotateOnClick : MonoBehaviour
{
    public Image image;

    private Quaternion initialRotation;

    [HideInInspector]
    public Quaternion targetRotation;

    public float targetZRotation;
    private float rotationTime = 1f;

    private bool isRotating = false;

    private void Start()
    {
        initialRotation = InitialRotation();

        targetRotation = TargetRotation();
    }

    public void Rotate()
    {
        if (!isRotating)
        {
            initialRotation = image.transform.rotation;

            targetRotation = TargetRotation();

            StartCoroutine(RotateCoroutine());
        }
    }

    private IEnumerator RotateCoroutine()
    {
        isRotating = true;

        float elapsedTime = 0f;

        while (elapsedTime < rotationTime)
        {
            elapsedTime += Time.deltaTime;

            float t = Mathf.Clamp01(elapsedTime / rotationTime);

            image.transform.rotation = Quaternion.Slerp(initialRotation, targetRotation, t);

            yield return null;
        }

        isRotating = false;
    }

    Quaternion InitialRotation()
    {
        return image.transform.rotation;
    }
    Quaternion TargetRotation()
    {
        return image.transform.rotation * Quaternion.Euler(0f,0f, -targetZRotation);
    }

    public void SolvePuzzle()
    {
        image.transform.eulerAngles = Vector3.zero;
    }
}
