using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    public List<RotateOnClick> images = new List<RotateOnClick>();
    public List<RotatingCheck> deactivate = new List<RotatingCheck>();
    public List<Image> planets = new List<Image>();

    public int[] nGiri = new int[3];

    private void Awake()
    {
        foreach (var planet in planets)
        {
            planet.color = Color.grey;
        }
    }

    public void OnButtonPressed()
    {
        for (int i = 0; i < images.Count; i++)
        {
            images[i].targetZRotation = nGiri[i];
            images[i].Rotate();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        foreach (var planet in planets)
        {
            planet.color = Color.white;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        foreach (var planet in planets)
        {
            planet.color = Color.grey;
        }
    }
}