using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FeedbackPassword : MonoBehaviour
{
    List<int> password = new List<int>();

    public TextMeshProUGUI myText;

    public void AddSolution(int digit1, int digit2, int digit3, int digit4)
    {
        password.Add(digit1);
        password.Add(digit2);
        password.Add(digit3);
        password.Add(digit4);

        myText.text = $"{digit1}{digit2}{digit3}{digit4}";
    }
}
