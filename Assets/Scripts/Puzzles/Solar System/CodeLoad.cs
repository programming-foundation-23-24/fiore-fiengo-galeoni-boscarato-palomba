using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeLoad : MonoBehaviour
{
    public RotatingPuzzleSolution solSys;

    public void SolvePuzzle()
    {
        StartCoroutine(DelaySolvePuzzle());
    }

    IEnumerator DelaySolvePuzzle()
    {
        yield return new WaitForEndOfFrame();

        solSys.DisableButtons();
        solSys.codeText.text = $"{solSys.tastierino.solution[0]}{solSys.tastierino.solution[1]}{solSys.tastierino.solution[2]}{solSys.tastierino.solution[3]}";
    }
}
