using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class RotatingPuzzleSolution : MonoBehaviour
{
    public UnityEvent OnPuzzleSolved;

    public List<GameObject> puzzleComponents = new List<GameObject>();

    [SerializeField] int solvedPiecesCount = 0;

    public float rotationTolerance = 0.1f;

    public TastierinoManager tastierino;

    public TextMeshProUGUI codeText;

    public List<Button> buttons;

    [SerializeField] bool isRotating;

    public FeedbackPassword feedbackPassword;

    public AudioSource completionSound;

    public void CheckForSolution()
    {
        if (!isRotating)
        {
            StartCoroutine(DelayCheck());
            isRotating = true;
            DisableButtons();
        }
    }

    IEnumerator DelayCheck()
    {
        yield return new WaitForSeconds(1.05f);
        IsSolved();
    }

    private void IsSolved()
    {
        foreach (GameObject puzzlePiece in puzzleComponents)
        {
            Vector3 pieceRotation = puzzlePiece.transform.eulerAngles;

            // Utilizza una tolleranza per considerare la possibilit� di leggere valori float non esatti
            if (Mathf.Abs(pieceRotation.z) < rotationTolerance)
            {
                solvedPiecesCount++;
            }
        }

        // Controlla se tutti i pezzi del puzzle hanno l'angolo Z a 0
        if (solvedPiecesCount == puzzleComponents.Count)
        {
            DisableButtons();

            CreateTastierinoSolution();

            OnPuzzleSolved?.Invoke();

            completionSound.Play();
        }
        else
        {
            // Resetta il contatore se il puzzle non � completamente risolto
            solvedPiecesCount = 0;
            EnableButtons();
        }

        isRotating = false;
    }

    void EnableButtons()
    {
        foreach (Button button in buttons)
        {
            button.interactable = true;
        }
    }
    public void DisableButtons()
    {
        foreach (Button button in buttons)
        {
            button.interactable = false;
        }
    }
    void CreateTastierinoSolution()
    {
        int digit1 = Random.Range(0, 9);
        int digit2 = Random.Range(0, 9);
        int digit3 = Random.Range(0, 9);
        int digit4 = Random.Range(0, 9);

        codeText.text = $"{digit1}{digit2}{digit3}{digit4}";

        tastierino.AddSolution(digit1, digit2, digit3, digit4);

        feedbackPassword.AddSolution(digit1, digit2, digit3, digit4);

        Debug.LogWarning("Puzzle Solved!");
        Debug.LogWarning($"{digit1}{digit2}{digit3}{digit4}");
    }
}