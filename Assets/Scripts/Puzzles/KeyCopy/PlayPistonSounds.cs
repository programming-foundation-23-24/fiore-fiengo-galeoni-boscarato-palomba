using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPistonSounds : MonoBehaviour
{
    public AudioSource source;

    public void PlayRattle()
    {
        source.Play();
    }
}
