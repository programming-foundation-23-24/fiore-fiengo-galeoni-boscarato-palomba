using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pistoncino : MonoBehaviour
{
    Image myPistoncinoImage;

    public bool isPistoncincinoInPosition;

    private void Awake()
    {
        myPistoncinoImage = GetComponentInChildren<Image>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isPistoncincinoInPosition = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isPistoncincinoInPosition = false;
    }
}
