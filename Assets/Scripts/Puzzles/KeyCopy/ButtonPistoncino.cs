using System.Collections;
using UnityEngine;

public class ButtonPistoncino : MonoBehaviour
{
    KeyCopyBrain brain;

    public bool isUpButton;

    public RectTransform myPistoncino;
    Animator myAnim;

    public RectTransform otherPistoncino;
    Animator otherAnim;

    public int maxHeight;
    public int minHeight;

    public int nChildSteps;
    public int step;

    private void Awake()
    {
        brain = GetComponentInParent<KeyCopyBrain>();

        myAnim = myPistoncino.GetComponentInChildren<Animator>();
        otherAnim = otherPistoncino.GetComponentInChildren<Animator>();
    }

    public void OnClick()
    {
        if (CanMove())
        {
            MovePistoncino();
            MoveOtherPistoncino();
            StartCoroutine(CheckSolutionNextFrame());
        }
        else
        {
            myAnim.SetTrigger("Play");
            otherAnim.SetTrigger("Play");
        }
    }

    bool CanMove()
    {
        bool withinHeightRange = isUpButton ? (CurrentPistoncinoHeight() + step <= maxHeight) : (CurrentPistoncinoHeight() - step >= minHeight);

        bool withinOtherHeightRange = (CurrentOtherPistoncinoHeight() + TotalChildMove()) >= minHeight && (CurrentOtherPistoncinoHeight() + TotalChildMove()) <= maxHeight;

        return withinHeightRange && withinOtherHeightRange;
    }

    void MovePistoncino()
    {
        float direction = isUpButton ? 1f : -1f;


        myPistoncino.anchoredPosition += new Vector2(0f, step * direction);

    }

    void MoveOtherPistoncino()
    {
        otherPistoncino.anchoredPosition += new Vector2(0f, TotalChildMove());
    }

    float CurrentPistoncinoHeight()
    {
        return myPistoncino.anchoredPosition.y;
    }

    float CurrentOtherPistoncinoHeight()
    {
        return otherPistoncino.anchoredPosition.y;
    }

    int TotalChildMove()
    {
        return step * nChildSteps;
    }

    IEnumerator CheckSolutionNextFrame()
    {
        yield return new WaitForSeconds(0.1f);
        CheckSolution();
    }
    void CheckSolution()
    {
        brain.CheckSolution();
    }
}
