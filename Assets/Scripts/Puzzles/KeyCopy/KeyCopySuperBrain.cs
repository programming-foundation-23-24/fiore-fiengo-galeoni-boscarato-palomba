using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCopySuperBrain : PuzzleSolution
{
    public KeyCopyBrain keyCopy;

    public void CheckForSolution()
    {
        if (keyCopy.puzzleSolved)
        {
            PuzzleSolved();
        }
    }
}
