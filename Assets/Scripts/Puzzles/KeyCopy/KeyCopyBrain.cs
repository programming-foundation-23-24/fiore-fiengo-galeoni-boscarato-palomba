using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class KeyCopyBrain : MonoBehaviour
{
    public List<Pistoncino> pistons = new List<Pistoncino>();

    public KeyCopySuperBrain brainSolution;

    public Button printButton;

    public Image printButtonImage;

    public Color resolvedColor = new Color();
    public Color notResolvedColor = new Color();

    bool canBeSolved;

    public bool puzzleSolved;

    public PuzzleInteraction myInteractable;

    private void Awake()
    {
        printButtonImage.color = Color.red;

        foreach (var piston in GetComponentsInChildren<Pistoncino>())
        {
            pistons.Add(piston);
        }
    }

    public void CheckSolution()
    {
        if (pistons.All(piston => piston.isPistoncincinoInPosition))
        {
            canBeSolved = true;

            printButton.interactable = true;

            printButtonImage.color = resolvedColor;

            if (canBeSolved)
            {
                myInteractable.canInteract = false;

                PuzzleSolved();
            }
        }
        else
        {
            printButton.interactable = false;

            printButtonImage.color = notResolvedColor;

            canBeSolved = false;
        }
    }

    void PuzzleSolved()
    {
        canBeSolved = false;

        puzzleSolved = true;
    }
}
