using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCell : MonoBehaviour
{
    public bool isActive;

    public List<BaseCell> cells = new List<BaseCell>();

    public GameObject intersection;

    public abstract void CheckIfActive();
}
