using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericParentActiveCheck : MonoBehaviour
{
    public List<Image> images = new List<Image>();
    public bool isActive;

    void Awake()
    {
        images.AddRange(GetComponentsInChildren<Image>(true));
    }

    void Update()
    {
        int activeCount = 0;

        foreach (Transform child in transform)
        {
            if (child.CompareTag("Active"))
            {
                activeCount++;
            }
        }

        if (activeCount > 0)
        {
            ChangeChildrenTagActive();
        }
        else
        {
            ChangeChildrenTagInactive();
        }
    }

    public void ChangeChildrenTagActive()
    {
        isActive = true;
        gameObject.tag = "Active";

        foreach (Transform child in transform)
        {
            child.tag = "Active";
        }

        foreach (Image image in images)
        {
            image.color = Color.blue;
        }
    }

    public void ChangeChildrenTagInactive()
    {
        isActive = false;
        gameObject.tag = "Inactive";

        foreach (Transform child in transform)
        {
            child.tag = "Inactive";
        }

        foreach (Image image in images)
        {
            image.color = Color.black;
        }
    }
}