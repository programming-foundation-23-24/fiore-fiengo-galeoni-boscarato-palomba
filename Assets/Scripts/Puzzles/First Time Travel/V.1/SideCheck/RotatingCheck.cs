using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Unity.VisualScripting;

public class RotatingCheck : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Active"))
        {
            gameObject.tag = collision.tag;
        }
        else 
        {
            gameObject.tag = "Inactive";
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("Collision Exit");
        gameObject.tag = "Inactive";
    }
}
