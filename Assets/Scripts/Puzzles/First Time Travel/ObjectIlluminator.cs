using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ObjectIlluminator : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    public List<Image> OBJ = new List<Image>();

    public Color ObjColorSelect = new Color();
    public Color ObjColorDeselect = new Color();

    private void Awake()
    {
        foreach (var obj in OBJ)
        {
            obj.color = ObjColorDeselect;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        foreach (var obj in OBJ)
        {
            obj.color = ObjColorSelect;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        foreach (var obj in OBJ)
        {
            obj.color = ObjColorDeselect;
        }
    }
}
