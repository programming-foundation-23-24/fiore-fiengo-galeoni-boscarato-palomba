using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PuzzleIsSolved : MonoBehaviour
{
    public UnityEvent SolvePuzzleOnLoad; 

    public void SolvePuzzle()
    {
        SolvePuzzleOnLoad?.Invoke();
    }
}
