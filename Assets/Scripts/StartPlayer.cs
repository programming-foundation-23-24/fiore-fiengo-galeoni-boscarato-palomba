using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPlayer : MonoBehaviour
{
    public void UnlockPlayer()
    {
        GameManager.Instance.StartPlayer();
        GameManager.Instance.PuzzleIsNotOnScreen();
    }
}
