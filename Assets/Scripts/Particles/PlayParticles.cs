using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayParticles : MonoBehaviour
{
    public List<ParticleSystem> myParticles = new List<ParticleSystem>();

    public void ParticlesPlay()
    {
        foreach (var particle in myParticles)
        {
            particle.Play();
        }
    }
}
