using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;

    DialogueManager dialogueManager;

    public BaseDialogueMemory myDialogueOnMemory;

    public bool releasePlayerAtTheEnd = true;

    private void Awake()
    {
        dialogueManager = FindObjectOfType<DialogueManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            TriggerDialogue();

            GameManager.Instance.PuzzleIsOnScreen();

            if (myDialogueOnMemory != null)
            {
                GameManager.Instance.DialogueCollected(myDialogueOnMemory.dialogueName, myDialogueOnMemory, gameObject);
            }
        }
    }

    public void TriggerDialogue()
    {
        dialogueManager.releasePlayerAtTheEnd = releasePlayerAtTheEnd;

        dialogueManager.StartDialogue(dialogue);

        gameObject.SetActive(false);

        GameManager.Instance.StopPlayer();
    }
    public void ContinueDialogue()
    {
        dialogueManager.DisplayNextSentence();
    }
    public void EndDialogue()
    {
        dialogueManager.EndDialogue(releasePlayerAtTheEnd);
    }
}
