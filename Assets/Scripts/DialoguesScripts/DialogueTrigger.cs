using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : BaseInteractable
{ 
    public Dialogue dialogue;

    DialogueManager dialogueManager;

    public BaseDialogueMemory myDialogueOnMemory;

    public bool releasePlayerAtTheEnd = true;

    public override void Awake()
    {
        base.Awake();

        dialogueManager = FindObjectOfType<DialogueManager>(); //incapsulato il singleton
    }

    public override void Action()
    {
        base.Action();

        TriggerDialogue();

        GameManager.Instance.DialogueCollected(myDialogueOnMemory.dialogueName, myDialogueOnMemory, gameObject);
    }

    public void TriggerDialogue()
    {
        dialogueManager.releasePlayerAtTheEnd = releasePlayerAtTheEnd;

        dialogueManager.StartDialogue(dialogue);

        gameObject.SetActive(false);
    }
    public void ContinueDialogue()
    {
        dialogueManager.DisplayNextSentence();
    }
    public void EndDialogue()
    {
        dialogueManager.EndDialogue(releasePlayerAtTheEnd);
    }
}
