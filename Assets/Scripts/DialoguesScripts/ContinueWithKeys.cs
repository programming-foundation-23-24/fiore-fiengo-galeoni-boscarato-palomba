using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueWithKeys : MonoBehaviour
{
    DialogueManager dialogueManager;

    private void Awake()
    {
        dialogueManager = GetComponentInParent<DialogueManager>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            dialogueManager.DisplayNextSentence();
        }
    }
}
