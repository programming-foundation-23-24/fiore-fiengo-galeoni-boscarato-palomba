using System;
using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    Queue<Sentence> sentences;

    public GameObject canvas;

    public TextMeshProUGUI Player_nameText;
    public TextMeshProUGUI dialogueText;
    
    public Animator animator;

    public float typeSpeed= 0.02f;
    
    Dialogue currentDialogue;

    public int currentSentenceIndex;

    public bool isTypingText;

    public bool releasePlayerAtTheEnd = true;

    void Start()
    {
        sentences = new Queue<Sentence>();
    }
    public void StartDialogue (Dialogue dialogue)
    {
        canvas.SetActive(true);

        currentDialogue = dialogue;

        currentSentenceIndex = 0;
        OpenDialogueBox();
        DisplayNextSentence();
    }


    public void DisplayNextSentence()
    {
        if (IsDialogueOver() == false)
        {
            if(isTypingText)
            {
                StopAllCoroutines();
                dialogueText.text = currentDialogue.sentences[currentSentenceIndex - 1].text;
                isTypingText = false;
            }
            else
            {
                ShowSentenceInUI(Sentence());
                if (IsThereEvent())
                {
                    ExecuteSentenceEvent();

                }
                currentSentenceIndex++;
            } 
        }
        else
        {
            if (isTypingText)
            {
                StopAllCoroutines();
                dialogueText.text = currentDialogue.sentences[currentSentenceIndex-1].text;
                isTypingText = false;
            }
            else 
            {
                EndDialogue(releasePlayerAtTheEnd);
                CloseDialogueBox();
            }
            
        }
    }

    private void OpenDialogueBox()
    {
        animator.SetBool("IsOpen", true);
    }
    private void CloseDialogueBox()
    {
        animator.SetBool("IsOpen", false);
    }

    private void ExecuteSentenceEvent()
    {
        currentDialogue.sentences[currentSentenceIndex].onEndSentence.Invoke();
    }

    private bool IsThereEvent()
    {
        return currentDialogue.sentences[currentSentenceIndex].onEndSentence != null;
    }

    private bool IsDialogueOver()
    {
        return !(currentSentenceIndex < currentDialogue.sentences.Length);
    }

    private Sentence Sentence()
    {
        return currentDialogue.sentences[currentSentenceIndex];
    }

    private void ShowSentenceInUI(Sentence sentence)
    {
        Player_nameText.text = sentence.whoIsTalking;
       
        

        
        StartCoroutine(TypeSentence(sentence.text));
    }

    IEnumerator TypeSentence (string sentence)
    {
        isTypingText = true;

        dialogueText.text = " ";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;

            yield return new WaitForSeconds(typeSpeed);
        }

        isTypingText = false;
    }

    public void ExitDialogue()
    {
        sentences.Clear(); // Rimuovi tutte le frasi rimanenti
        EndDialogue(true);
    }

    public void EndDialogue(bool releasePlayer)
    {
        if (currentDialogue.OnDialogueOver != null)
        {
            currentDialogue.OnDialogueOver.Invoke();
        }

        if (releasePlayer)
        {
            GameManager.Instance.StartPlayer();

            GameManager.Instance.PuzzleIsNotOnScreen();
        }

        animator.SetBool("IsOpen", false);

        Debug.Log("ho finito il dialogo");
    }
}