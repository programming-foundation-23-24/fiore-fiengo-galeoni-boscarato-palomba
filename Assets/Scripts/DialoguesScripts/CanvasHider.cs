using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasHider : MonoBehaviour
{
    Canvas myCanvas;

    private void Awake()
    {
        myCanvas = GetComponentInParent<Canvas>();
    }

    public void HideCanvas()
    {
        myCanvas.gameObject.SetActive(false);
    }
}
