using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraConfiner : MonoBehaviour
{
    public CinemachineConfiner confiner;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Confiner"))
        {
            BoxCollider[] colliders = other.GetComponentsInChildren<BoxCollider>();
            confiner.m_BoundingVolume = colliders[1];
        }
    }
}
