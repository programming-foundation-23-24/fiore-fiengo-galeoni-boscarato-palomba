using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panorama : BaseInteractable
{
    Camera panoramaCamera;

    public GameObject exitPanorama;

    bool hasInteracted;

    public override void Awake()
    {
        base.Awake();

        panoramaCamera = GetComponentInChildren<Camera>();
    }

    public override void Update()
    {
        base.Update();

        if (Input.GetButtonDown("Pause") && hasInteracted)
        {
            Action();
            Debug.Log("ActionFired");
        }
    }

    public override void Action()
    {
        base.Action();

        hasInteracted = !hasInteracted;

        panoramaCamera.enabled = !panoramaCamera.enabled;

        if(!panoramaCamera.enabled)
        {
            GameManager.Instance.StartPlayer();
            GameManager.Instance.PuzzleIsNotOnScreen();

            exitPanorama.SetActive(false);
        }
        else
        {
            exitPanorama.SetActive(true);
        }
    }
}
