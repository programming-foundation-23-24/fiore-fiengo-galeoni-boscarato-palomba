using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TastierinoLightChange : MonoBehaviour
{
    Light halo;

    public Color defaultColor = Color.red;
    public Color unlockedColor = Color.green;

    private void Awake()
    {
        halo = GetComponent<Light>();
        halo.color = defaultColor;
    }
    public void ChangeLightColor()
    {
        halo.color = unlockedColor;
    }
}
