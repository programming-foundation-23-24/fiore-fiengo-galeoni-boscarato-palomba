using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbObstacleWithCondition : BaseInteractable
{
    public Dialogue dialogue;
    DialogueManager dialogueManager;

    public bool canClimb;

    public Transform climbDestination;
    public GameObject otherClimb;

    public override void Awake()
    {
        base.Awake();

        dialogueManager = FindObjectOfType<DialogueManager>(); //incapsulato il singleton
    }

    public override void Action()
    {
        if (!canClimb)
        {
            base.Action();
            TriggerDialogue();
        }
        else
        {
            StartCoroutine(DisableClimb());
            GameManager.Instance.Player.transform.position = climbDestination.position;
        }
    }

    public void CanClimb()
    {
        canClimb = true;
    }


    public void TriggerDialogue()
    {
        dialogueManager.releasePlayerAtTheEnd = true;

        dialogueManager.StartDialogue(dialogue);

        canInteract = false;
    }
    public void ContinueDialogue()
    {
        dialogueManager.DisplayNextSentence();
    }
    public void EndDialogue()
    {
        dialogueManager.EndDialogue(true);
    }

    IEnumerator DisableClimb()
    {
        otherClimb.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        otherClimb.SetActive(true);
    }
}
