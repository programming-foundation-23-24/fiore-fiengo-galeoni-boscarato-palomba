using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StrangeMachine : BaseInteractable
{
    public UnityEvent OnInteracted;
    public override void Action()
    {
        OnInteracted?.Invoke();

        if (customPrompt != null)
        {
            GameManager.Instance.HideCustomPrompt(customPrompt);
        }
        else
        {
            GameManager.Instance.HideDefaultPrompt();
        }

        gameObject.SetActive(false);
    }

    public void ExternalActivation()
    {
        OnInteracted?.Invoke();
    }
}
