using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ONOFFShadow : MonoBehaviour
{
    public GameObject myShadow;

    public float duration;

    public float currentValue;

    private Renderer rend;
    private MaterialPropertyBlock materialPropertyBlock;

    void Start()
    {
        rend = GetComponentInChildren<Renderer>();
        materialPropertyBlock = new MaterialPropertyBlock();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(StartInterpolation());
        }
    }

    IEnumerator StartInterpolation()
    {
        float elapsedTime = 0f;

        // Esegui l'interpolazione finch� il tempo trascorso � inferiore alla durata desiderata
        while (elapsedTime < duration)
        {
            // Incrementa il tempo trascorso
            elapsedTime += Time.deltaTime;

            // Calcola il nuovo valore usando Mathf.Lerp
            // Modifica dinamicamente la trasparenza nel tempo
            float newAlpha = Mathf.Lerp(1, 0, elapsedTime / duration);

            // Imposta la nuova trasparenza nel MaterialPropertyBlock
            materialPropertyBlock.SetFloat("_Mode", 3); // Imposta la modalit� del materiale su "Fade"
            materialPropertyBlock.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            materialPropertyBlock.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            materialPropertyBlock.SetInt("_ZWrite", 0);

            // Imposta l'alpha nel MaterialPropertyBlock
            Color color = rend.material.color;
            color.a = newAlpha;
            materialPropertyBlock.SetColor("_Color", color);

            // Applica il MaterialPropertyBlock al renderizzatore
            rend.SetPropertyBlock(materialPropertyBlock);

            // Puoi fare qualcos'altro con il valore corrente qui
            // ...

            // Attendere il frame successivo prima di continuare
            yield return null;
        }

        myShadow.SetActive(false);
    }
}
