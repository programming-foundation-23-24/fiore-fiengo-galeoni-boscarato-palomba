using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class BaseInteractable : MonoBehaviour
{
    public UnityEvent OnEnterTrigger, OnExitTrigger;

    public bool canInteract;

    GameObject promptCanvas;
    [HideInInspector]
    public GameObject customPrompt;

    //public bool stillInteractable = true;

    [Header("Leave empty for defaut (E - Interact)")]
    public string PromptGameobjectName;

    public virtual void Awake()
    {
        promptCanvas = GameObject.Find("PromptCanvas");

        Transform custom = promptCanvas.transform.Find(PromptGameobjectName);

        customPrompt = custom.gameObject;
    }
    public virtual void Update()
    {
        if (Input.GetButtonDown("Interact") && canInteract)
        {
            Action();
        }

        if (Input.GetButtonDown("DimensionChange") && canInteract)
        {
            canInteract = false;
        }
    }

    public virtual void Action()
    {
        GameManager.Instance.StopPlayer();
        GameManager.Instance.PuzzleIsOnScreen();
        
        HidePrompt();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canInteract = true;

            OnEnterTrigger?.Invoke();

            ShowPrompt();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canInteract = false;

            OnExitTrigger?.Invoke();

            HidePrompt();
        }
    }

    public void HidePrompt()
    {
        if (PromptGameobjectName != "")
        {
            GameManager.Instance.HideCustomPrompt(customPrompt);
        }
        else
        {
            GameManager.Instance.HideDefaultPrompt();
        }
    }
    public void ShowPrompt()
    {
        if (PromptGameobjectName != "")
        {
            GameManager.Instance.ShowCustomPrompt(customPrompt);
        }
        else
        {
            GameManager.Instance.ShowDefaultPrompt();
        }
    }

    /*public bool StillInteractable()
    {
        return stillInteractable;
    }*/
}
