using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LockedDoorInteraction : BaseInteractable
{
    public UnityEvent OnInteracted;
    
    [SerializeField] Animator doorAnim;

    [SerializeField] bool isOpen;
    public bool locked = true;

    public GameObject myAutoDialogue;

    public override void Action()
    {
        if (locked)
        {
            myAutoDialogue.SetActive(true);

            gameObject.SetActive(false);

            GameManager.Instance.HideDefaultPrompt();
        }
        else
        {
            if (!isOpen)
            {
                doorAnim.SetTrigger("OpenDoor");
                isOpen = true;
                gameObject.SetActive(false);
                GameManager.Instance.HideDefaultPrompt();
                OnInteracted?.Invoke();
            }
        }
    }

    public void UnlockExitDoor()
    {
        locked = false;
    }
}
