using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleInteraction : BaseInteractable
{
    public GameObject puzzle;

    public override void Action()
    {
        base.Action();

        puzzle.SetActive(true);
    }

    public void DeactivateTrigger()
    {
        canInteract = false;

        GameManager.Instance.HideAllPrompts();

        gameObject.SetActive(false);
    }
}
