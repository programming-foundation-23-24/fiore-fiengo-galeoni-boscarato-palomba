using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CUTSCENEManager : MonoBehaviour
{
    public Animator animator;

    public UnityEvent OnCutsceneEnded;


    [Header("Se true, ricordarsi di eseguire")]
    [Header("un'animation event con la funzione EnablePlayer()")]
    [Space]
    public bool freezePlayerForCutscene;

    Collider trigger;

    private void Awake()
    {
        trigger = GetComponent<Collider>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.Instance.PuzzleIsOnScreen();

            if (freezePlayerForCutscene)
            {
                GameManager.Instance.StopPlayer();

                animator.SetTrigger("Play");
                StartCoroutine(PlayAgain());
                GameManager.Instance.gameStarted = false;
            }
            else
            {
                animator.SetTrigger("Play");

                DisableCollider();
            }
        }
    }

    public void EnablePlayer()
    {
        GameManager.Instance.StartPlayer();
        GameManager.Instance.gameStarted = true;

        GameManager.Instance.PuzzleIsNotOnScreen();
    }

    public void DisableCollider()
    {
        trigger.enabled = false;
    }

    public void OnCutSceneEnded()
    {
        OnCutsceneEnded?.Invoke();
    }

    IEnumerator PlayAgain()
    {
        yield return new WaitForEndOfFrame();
        animator.SetTrigger("Play");
    }


}
