using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteraction : BaseInteractable
{
    [SerializeField] Animator doorAnim;

    [SerializeField] bool isOpen;
    public override void Action()
    {
        if (!isOpen)
        {
            doorAnim.SetTrigger("OpenDoor");
            isOpen = true;
            gameObject.SetActive(false);
            GameManager.Instance.HideDefaultPrompt();
        }
    }
}
