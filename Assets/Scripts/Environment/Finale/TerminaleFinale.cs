using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminaleFinale : BaseInteractable
{
    public GameObject CUTSCENE;

    public override void Action()
    {
        base.Action();

        CUTSCENE.SetActive(true);

        GameManager.Instance.Player.SetActive(false);
    }
}
