using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Unity.VisualScripting.Member;

public class SoundPlayerTrigger : MonoBehaviour
{
    public AudioSource source;
    public SoundPlayerTrigger source2;
    public float fadeDuration = 2f;
    private bool isFading = false;

    public bool fullVolumeOnAwake;

    private void Awake()
    {
        source = GetComponent<AudioSource>();

        if (!fullVolumeOnAwake)
        {
            source.volume = 0f;
        }

        source.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlaySound();

            if(source2 != null)
            {
                source2.StopSound();
            }
        }

    }

    public void PlaySound()
    {
        if (!isFading)
        {
            isFading = true;
            StartCoroutine(FadeAudio());
        }
    }

    public void StopSound()
    {
        if (!isFading)
        {
            isFading = true;
            StartCoroutine(FadeAudio());
        }
    }

    IEnumerator FadeAudio()
    {
        float startVolume = source.volume;
        float targetVolume = (startVolume == 0.0f) ? 1.0f : 0.0f;

        float startTime = Time.time;

        while (Time.time < startTime + fadeDuration)
        {
            source.volume = Mathf.Lerp(startVolume, targetVolume, (Time.time - startTime) / fadeDuration);
            yield return null;
        }

        source.volume = targetVolume;
        isFading = false;
    }

    public void HardStop()
    {
        source.volume = 0;
        source.Stop();
    }

}
