using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundBrain : MonoBehaviour
{
    public List<SoundPlayerTrigger> allAudios = new List<SoundPlayerTrigger>();


    private void Awake()
    {
        foreach (var audio in GetComponentsInChildren<SoundPlayerTrigger>())
        {
            allAudios.Add(audio);
        }
    }

    public void HardSoundStop()
    {
        foreach (SoundPlayerTrigger audio in allAudios)
        {
            audio.HardStop();
        }
    }
}
