using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSound : MonoBehaviour
{
    public AudioSource source;

    public void PlayOpenDoorSound()
    {
        source.Play();
    }
}
