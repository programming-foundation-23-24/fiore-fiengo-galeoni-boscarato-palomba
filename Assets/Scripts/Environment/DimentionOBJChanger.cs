using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DimentionOBJChanger : MonoBehaviour
{
    public GameObject myPresent;
    public GameObject myFuture;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.Instance.DimentionOBJChange(myPresent, myFuture);

            gameObject.SetActive(false);
        }
    }
}
