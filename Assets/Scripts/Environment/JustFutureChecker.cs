using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class JustFutureChecker : MonoBehaviour
{
    public UnityEvent OnTriggered;

    public bool ignoreSave;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !ignoreSave)
        {
            if (GameManager.Instance.futureKnobsAndCables == 1)
            {
                OnTriggered?.Invoke();
            }
        }
    }
}
