using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DestructibleObject : MonoBehaviour
{
    public UnityEvent OnDestroyed;

    public GameObject myParticles;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            Instantiate(myParticles, gameObject.transform.position, Quaternion.identity);

            OnDestroyed?.Invoke();

            gameObject.SetActive(false);
        }
    }
}
