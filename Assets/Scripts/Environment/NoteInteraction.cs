using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class NoteInteraction : BaseInteractable
{
    public UnityEvent OnInteracted;

    public GameObject noteOnCanvas;

    public BaseNoteMemory myNoteOnMemory;

    public AudioSource source;

    public override void Action()
    {
        base.Action();

        noteOnCanvas.SetActive(true);

        StartCoroutine(DelayNoteDeactivation());

        OnInteracted?.Invoke();

        if (myNoteOnMemory != null)
        {
            WriteContentOnCanvas();

            GameManager.Instance.NoteCollected(myNoteOnMemory.noteName, myNoteOnMemory, this);
        }
    }

    public void WriteContentOnCanvas()
    {
        noteOnCanvas.GetComponentInChildren<TextMeshProUGUI>().text = myNoteOnMemory.noteContent;
    }

    IEnumerator DelayNoteDeactivation()
    {
        canInteract = false;

        if(source != null)
        {
            source.Play();
        }
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
    }
}
