using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionLightAnim : MonoBehaviour
{
    Light mylight;

    public float maxRange;
    public float minRange;

    bool turnandre;

    public float multiplier;

    public bool isNote;
    Color noteColor = Color.red;

    public bool isPuzzle;
    Color puzzleColor = new Color(1, 0.5f, 0, 1);

    public bool isDimentionChange;
    Color dimentionChangeColor = new Color(0.5f, 0.3f, 1, 1);

    public bool isSave;
    Color saveColor = new Color(0.75f, 1, 0.3f, 1);

    Color defaultColor = new Color(0.8f, 1, 1, 1);

    private void Awake()
    {
        mylight = GetComponent<Light>();

        if (isNote)
        {
            mylight.color = noteColor;
        }
        else if (isPuzzle)
        {
            mylight.color = puzzleColor;
        }
        else if (isDimentionChange)
        {
            mylight.color = dimentionChangeColor;
        }
        else if (isSave)
        {
            mylight.color = saveColor;
        }
        else
        {
            mylight.color = defaultColor;
        }
    }

    private void Update()
    {
        if (mylight.range < minRange)
        {
            turnandre = false;
        }
        else if (mylight.range > maxRange)
        {
            turnandre = true;
        }

        if (!turnandre)
        {
            mylight.range += Time.deltaTime * multiplier;
        }
        else
        {
            mylight.range -= Time.deltaTime * multiplier;
        }
    }
}
