using UnityEngine;

public class FlickeringLight : MonoBehaviour
{
    public float minIntensity = 0.5f;
    public float maxIntensity = 1.5f;
    public float flickerSpeed = 1f;

    private Light myLight;

    void Start()
    {
        myLight = GetComponent<Light>();

        InvokeRepeating("Flicker", 0f, flickerSpeed);
    }

    void Flicker()
    {
        float randomIntensity = Random.Range(minIntensity, maxIntensity);

        myLight.intensity = randomIntensity;
    }
}
