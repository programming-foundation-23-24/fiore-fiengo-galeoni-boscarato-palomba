using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SaveGame : BaseInteractable
{
    //public int currentSceneIndex;

    //public Transform saveLocation;

    //public int noteIndex;
    //public int dialogueIndex;

    //public List<RamPuzzlesSolved> puzzlesSolved = new List<RamPuzzlesSolved>();

    public GameObject interactionLight;

    Collider myCollider;

    public override void Awake()
    {
        base.Awake();

        myCollider = GetComponent<Collider>();
    }

    public override void Action()
    {
        StartCoroutine(SaveCooldown());

        Save();

        HidePrompt();
    }
    public void Save()
    {
        DataPersistenceManager.instance.SaveGame();

        Feedback();

        PlayerPrefs.SetInt("HaveSaved", 1);
        DataPersistenceManager.instance.haveSaved = PlayerPrefs.GetInt("HaveSaved");
    }
    void Feedback()
    {
        interactionLight.SetActive(false);

        GameManager.Instance.ShowAndHideFeedback();
    }

    IEnumerator SaveCooldown()
    {
        myCollider.enabled = false;

        canInteract = false;

        yield return new WaitForSeconds(2.5f);
        
        myCollider.enabled = true;

        interactionLight.SetActive(true);
    }
}
