using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SaveManager : DontDestroyOnLoad
{
    public static SaveManager Instance;

    [Header("Player Data")]
    public GameObject player;

    public Vector3 lastSaveLocation;
    public Vector3 defaultRespawnLocation;


    [Header("Save State")]
    public int haveSaved = 0;

    public bool RESETSAVE;

    #region REFERENCES
    private void Start()
    {
        ///Debug///
        if (RESETSAVE)
        {
            PlayerPrefs.SetInt("HaveSaved", 0);
        }
        ///Debug///


        InstanceManager();

        GetPlayerRef();

        haveSaved = PlayerPrefs.GetInt("HaveSaved");

        lastSaveLocation = new Vector3(PlayerPrefs.GetFloat("PlayerLocationX"), PlayerPrefs.GetFloat("PlayerLocationY"), PlayerPrefs.GetFloat("PlayerLocationZ"));

        if (lastSaveLocation == Vector3.zero)
        {
            lastSaveLocation = defaultRespawnLocation;
        }
    }
    private void InstanceManager()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void GetPlayerRef()
    {
        player = PlayerRef();
    }
    public GameObject PlayerRef()
    {
        return GameObject.FindGameObjectWithTag("Player");
    }
    #endregion
    /*#region MAIN MENU
    public void Continue()
    {
        SceneManager.LoadScene(1);
    }

    public void NewGame()
    {
        PlayerPrefs.SetInt("HaveSaved", 0);
        haveSaved = PlayerPrefs.GetInt("HaveSaved");

        lastSaveLocation = Vector3.zero;
        PlayerPrefs.SetFloat("PlayerLocationX", lastSaveLocation.x);
        PlayerPrefs.SetFloat("PlayerLocationY", lastSaveLocation.y);
        PlayerPrefs.SetFloat("PlayerLocationZ", lastSaveLocation.z);

        SceneManager.LoadScene(1);
    }
    #endregion
    #region IN GAME SAVE && RESPAWN

    public void Save()
    {
        PlayerPrefs.SetInt("HaveSaved", 1);
        haveSaved = PlayerPrefs.GetInt("HaveSaved");
        if (player == null)
        {
            GetPlayerRef();
        }

        lastSaveLocation = player.transform.position;

        PlayerPrefs.SetFloat("PlayerLocationX", lastSaveLocation.x);
        PlayerPrefs.SetFloat("PlayerLocationY", lastSaveLocation.y);
        PlayerPrefs.SetFloat("PlayerLocationZ", lastSaveLocation.z);

    }
    #endregion*/

    public void Respawn()
    {
        if (player != null)
        {
            player.transform.position = lastSaveLocation;
            GameManager.Instance.ResetPlayerAfterRespawn();
        }
        else
        {
            Debug.LogError("Player Reference For Respawn Missing!");
        }
    }
    
    #region ON SCENE LOADED

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Questo metodo verr� chiamato ogni volta che una scena viene caricata
        // Puoi chiamare qui il metodo che vuoi eseguire quando la scena � caricata
        // MetodoDaEseguireDopoIlCaricamento();
        Debug.Log("La scena � stata caricata: " + scene.name);

        GetPlayerRef();

        if (haveSaved == 1)
        {
            Respawn();
        }
    }
    #endregion
}
