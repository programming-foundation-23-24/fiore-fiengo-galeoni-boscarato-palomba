using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerDontDestroy : MonoBehaviour
{
    static GameManagerDontDestroy instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }
}
