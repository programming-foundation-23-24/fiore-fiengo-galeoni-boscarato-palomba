using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotesDontDestroy : MonoBehaviour
{
    static NotesDontDestroy instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }
}
