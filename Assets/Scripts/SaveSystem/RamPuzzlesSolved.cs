﻿using System.Collections.Generic;
using UnityEngine;

public class RamPuzzlesSolved : MonoBehaviour
{
    public static RamPuzzlesSolved Instance;

    public List<bool> puzzlesSolved;

    private void Start()
    {
        InstanceManager();
    }
    private void InstanceManager()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
