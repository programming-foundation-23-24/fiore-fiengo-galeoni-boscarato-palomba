using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TELEPORTER : MonoBehaviour
{
    public Transform destination;

    public Animator animator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(WaitForTeleport(other));
        }
    }

    IEnumerator WaitForTeleport(Collider other)
    {
        GameManager.Instance.StopPlayer();

        animator.SetTrigger("Play");

        yield return new WaitForSeconds(0.4f);

        other.transform.position = destination.position;

        yield return new WaitForSeconds(0.4f);

        GameManager.Instance.StartPlayer();
    }
}
