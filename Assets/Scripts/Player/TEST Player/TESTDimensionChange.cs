using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TESTDimensionChange : MonoBehaviour
{
    [SerializeField] GameObject present;
    [SerializeField] GameObject future;

    [SerializeField] bool presentDimension = true;

    private void Update()
    {
        if (Input.GetButtonDown("DimensionChange"))
        {
            StartCoroutine(DelayDimensionChange());
        }
    }

    IEnumerator DelayDimensionChange()
    {
        yield return new WaitForEndOfFrame();

        if (presentDimension)
        {
            present.SetActive(false);
            future.SetActive(true);
            presentDimension = false;
        }
        else
        {
            present.SetActive(true);
            future.SetActive(false);
            presentDimension = true;
        }

        Debug.Log("DimensionChanged");
    }
}
