using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAimShoot : MonoBehaviour
{
    public bool shootingUnlocked;

    [SerializeField] GameObject reticle;
    [SerializeField] BaseProjectile projectile;

    [SerializeField] Camera cam;
    public bool canShoot;

    public float shootCooldown;
    public bool cooldown;

    public float rotationSpeed;
    public GameObject laser;

    [Header("CameraShake")]
    public float shakeIntensity;
    public float shakeTime;

    public bool scriptIsActive;

    private void Update()
    {
        if (scriptIsActive)
        {
            if (shootingUnlocked)
            {
                GetMousePosition();

                Aim();
                Shoot();
            }
        }
    }

    private void GetMousePosition()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (canShoot)
            {
                Vector3 mousePos = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                transform.LookAt(mousePos);
            }
        }
    }

    private void Aim()
    {
        if (Input.GetButton("Fire2"))
        {
            canShoot = true;
        }
        else
        {
            canShoot = false;
        }
    }

    private void Shoot()
    {
        if (Input.GetButtonDown("Fire1") && canShoot && !cooldown)
        {
            CameraShakeController.Instance.ShakeCamera(shakeIntensity, shakeTime);

            StartCoroutine(ShootCooldown());
            BaseProjectile bullet = Instantiate(projectile, reticle.transform.position, Quaternion.Euler(90f, ShooterRotation(), 0f));
            bullet.rb.velocity = transform.forward * bullet.speed;
        }
    }
    float ShooterRotation()
    {
        return transform.rotation.eulerAngles.y;
    }

    IEnumerator ShootCooldown()
    {
        cooldown = true;
        yield return new WaitForSeconds(shootCooldown);
        cooldown = false;
    }
}
