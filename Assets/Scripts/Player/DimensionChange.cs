using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class DimensionChange : MonoBehaviour
{
    public GameObject present;
    public GameObject future;

    [SerializeField] bool canDimensionChange;
    [SerializeField] GameObject dimensionPrompt;
    [SerializeField] bool presentDimension = true;

    [SerializeField] bool cooldown;
    [SerializeField] float cooldownInterval;

    [SerializeField] GameObject lightFeedback;

    public Animator fade;

    public AudioSource source;

    public bool scriptIsActive;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (scriptIsActive)
        {
            if (Input.GetButtonDown("DimensionChange") && canDimensionChange && !IsPuzzleOnScreen() && !cooldown)
            {
                StartCoroutine(DelayDimensionChange());
                source.Play();
            }
        }
    }

    IEnumerator DelayDimensionChange()
    {
        StartCoroutine(DimensionChangeCooldown());

        fade.SetTrigger("Play");

        yield return new WaitForSeconds(0.3f);

        if (presentDimension)
        {
            present.SetActive(false);
            future.SetActive(true);
            presentDimension = false;
        }
        else
        {
            present.SetActive(true);
            future.SetActive(false);
            presentDimension = true;
        }

        NOChangeDimention();

        GameManager.Instance.HideAllPrompts();

        Debug.Log("DimensionChanged");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("TriggerDimension"))
        {
            YESChangeDimention();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("TriggerDimension"))
        {
            NOChangeDimention();
        }
    }

    void NOChangeDimention()
    {
        canDimensionChange = false;
        dimensionPrompt.SetActive(false);
        lightFeedback.SetActive(false);
    }
    void YESChangeDimention()
    {
        canDimensionChange = true;
        dimensionPrompt.SetActive(true);
        lightFeedback.SetActive(true);
    }

    public bool IsPuzzleOnScreen()
    {
        return GameManager.Instance.isPuzzleOnScreen;
    }

    IEnumerator DimensionChangeCooldown()
    {
        cooldown = true;
        yield return new WaitForSeconds(cooldownInterval);
        cooldown = false;
    }
}