using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePlayerTransform : MonoBehaviour, IDataPersistence
{
    public void LoadData(GameData gameData)
    {
        transform.position = gameData.playerPosition;
    }

    public void SaveData(GameData gameData)
    {
        gameData.playerPosition = transform.position;
    }
}
