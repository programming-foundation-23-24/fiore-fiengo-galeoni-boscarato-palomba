using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TESTCameraChange : MonoBehaviour
{
    public Camera cam;
    public Camera rayCam;

    public bool switched;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J) && !switched)
        {
            cam.enabled = false;
            rayCam.enabled = true;

            switched = true;
        }
        else if (Input.GetKeyDown(KeyCode.J) && switched)
        {
            cam.enabled = true;
            rayCam.enabled = false;

            switched = false;
        }
    }
}
