using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public PlayerAimShoot aimShoot;

    [SerializeField] Rigidbody rb;
    [SerializeField] float movementSpeed;
    [SerializeField] float walkSpeed;
    [SerializeField] float aimSpeed;

    float horizontalMovement;
    float verticalMovement;

    public Animator animator;

    public bool scriptIsActive;

    private void Update()
    {
        if (scriptIsActive)
        {
            Movement();
        }
        else
        {
            IdleAnim();
        }
    }

    private void Movement()
    {
        GetMovement();

        if (IfMoving())
        {
            rb.velocity = new Vector3(horizontalMovement, 0, verticalMovement).normalized * movementSpeed;

            CheckRotationAndWalkSpeed();

            WalkAnim();
        }
        else
        {
            IdleAnim();
        }
    }

    void GetMovement()
    {
        horizontalMovement = Input.GetAxisRaw("Horizontal");
        verticalMovement = Input.GetAxisRaw("Vertical");
    }

    bool IfMoving()
    {
        return (horizontalMovement != 0 || verticalMovement != 0);
    }

    void CheckRotationAndWalkSpeed()
    {
        if (!aimShoot.canShoot)
        {
            MovementRotationLogic(walkSpeed);
        }
        else
        {
            MovementRotationLogic(aimSpeed);
        }
    }

    void IdleAnim()
    {
        animator.SetTrigger("Idle"); 
    }
    void WalkAnim()
    {
        animator.SetTrigger("Walk");
    }

    void MovementRotationLogic(float walkSpeed)
    {
        movementSpeed = walkSpeed;

        if (!aimShoot.canShoot)
        {
            if (horizontalMovement == 0 && verticalMovement > 0)//W
            {
                transform.eulerAngles = Vector3.zero;
            }
            else if (horizontalMovement < 0 && verticalMovement > 0)//WA
            {
                transform.eulerAngles = new Vector3(0, -45, 0);
            }
            else if (horizontalMovement < 0 && verticalMovement == 0)//A
            {
                transform.eulerAngles = new Vector3(0, -90, 0);
            }
            else if (horizontalMovement < 0 && verticalMovement < 0)//SA
            {
                transform.eulerAngles = new Vector3(0, -135, 0);
            }
            else if (horizontalMovement == 0 && verticalMovement < 0)//S
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
            }
            else if (horizontalMovement > 0 && verticalMovement < 0)//SD
            {
                transform.eulerAngles = new Vector3(0, 135, 0);
            }
            else if (horizontalMovement > 0 && verticalMovement == 0)//D
            {
                transform.eulerAngles = new Vector3(0, 90, 0);
            }
            else if (horizontalMovement > 0 && verticalMovement > 0)//WD
            {
                transform.eulerAngles = new Vector3(0, 45, 0);
            }
        }
    }
}

