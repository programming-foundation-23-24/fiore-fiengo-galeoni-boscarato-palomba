using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PlayerHealth : MonoBehaviour, IDataPersistence
{
    public int currentHealth;
    public int maxHealth;

    public float defaultHealthRegenCooldown;

    public GameObject playerMesh;

    public List<MeshRenderer> allMeshRenderers = new List<MeshRenderer>();

    public Material defaultMaterial;
    public Material hurtMaterial;

    [Header("CameraShake")]
    public float shakeIntensity;
    public float shakeTime;

    [Header("Particles")]
    public ParticleSystem gotHitParticles;
    public ParticleSystem dripFirstHitParticles;
    public ParticleSystem dripSecondHitParticles;

    public VideoPlayer glitchFX;

    private void Start()
    {
        currentHealth = maxHealth;

        foreach (MeshRenderer renderer in GetComponentsInChildren<MeshRenderer>())
        {
            allMeshRenderers.Add(renderer);
        }
    }

    public void TakeDamage()
    {
        CameraShakeController.Instance.ShakeCamera(shakeIntensity, shakeTime);

        currentHealth -= 1;
        StopAllCoroutines();
        CheckIfDead();

        gotHitParticles.Play();
        ParticlesUpdate();

        if (isActiveAndEnabled)
        {
            StartCoroutine(ChangeMaterial());
        }
    }

    public void ParticlesUpdate()
    {
        if (currentHealth == maxHealth)
        {
            dripFirstHitParticles.Stop();
            dripSecondHitParticles.Stop();

            glitchFX.enabled = false;
        }
        else if (currentHealth == maxHealth - 1)
        {
            dripFirstHitParticles.Play();
            dripSecondHitParticles.Stop();

            glitchFX.enabled = true;
            glitchFX.playbackSpeed = 0.3f;
            glitchFX.targetCameraAlpha = 0.2f;
        }
        else if (currentHealth == maxHealth - 2)
        {
            dripFirstHitParticles.Play();
            dripSecondHitParticles.Play();

            glitchFX.enabled = true;
            glitchFX.playbackSpeed = 1f;
            glitchFX.targetCameraAlpha = 0.4f;
        }
    }

    public void RegenerateHealth()
    {
        currentHealth += 1;
        CheckIfFullHealth();
    }

    public void CheckIfFullHealth()
    {
        if (currentHealth < maxHealth)
        {
            StartCoroutine(WaitForHealthRegen());
        }
    }

    public void CheckIfDead()
    {
        if (currentHealth == 0)
        {
            playerMesh.SetActive(false);
            GameManager.Instance.DeathMenuToggle();
        }
        else
        {
            StartCoroutine(WaitForHealthRegen());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("DamageTrigger"))
        {
            TakeDamage();
            other.gameObject.SendMessage("Message");
        }
    }

    IEnumerator WaitForHealthRegen()
    {
        yield return new WaitForSeconds(defaultHealthRegenCooldown);
        
        RegenerateHealth();
        ParticlesUpdate();
    }

    IEnumerator ChangeMaterial()
    {
        foreach (MeshRenderer meshRenderer in allMeshRenderers)
        {
            meshRenderer.material = hurtMaterial;
        }

        yield return new WaitForSeconds(0.3f);

        foreach (MeshRenderer meshRenderer in allMeshRenderers)
        {
            meshRenderer.material = defaultMaterial;
        }
    }

    public void LoadData(GameData data)
    {
        GameManager.Instance.ResetPlayerAfterRespawn();
        
    }

    public void SaveData(GameData data)
    {
        return;
    }
}
