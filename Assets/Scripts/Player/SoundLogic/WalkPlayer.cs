using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkPlayer : MonoBehaviour
{
    public AudioSource source;

    public void PlayWalkSound()
    {
        source.Play();
    }
}
