using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMenuController : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            GameManager.Instance.PauseGame();
        } 
        
        if (Input.GetButtonDown("MemoryMenu") && !IsPuzzleOnScreen())
        {
            GameManager.Instance.MemoryMenu();
        }
    }

    public bool IsPuzzleOnScreen()
    {
        return GameManager.Instance.isPuzzleOnScreen;
    }
}
