using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISwitcher : MonoBehaviour
{
    [SerializeField] GameObject noteGrid;
    [SerializeField] GameObject dialogueGrid;

    [SerializeField] bool noteButton;

    public void OnClick()
    {
        if (noteButton)
        {
            noteGrid.SetActive(true);
            dialogueGrid.SetActive(false);
        }
        else
        {
            noteGrid.SetActive(false);
            dialogueGrid.SetActive(true);
        }
    }
}
