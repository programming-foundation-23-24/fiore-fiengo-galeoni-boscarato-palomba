using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitcher : MonoBehaviour
{
    public Camera mainCam;
    public Camera rayCam;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("CameraSwitcher"))
        {
            mainCam.enabled = false;
            rayCam.enabled = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("CameraSwitcher"))
        {
            mainCam.enabled = true;
            rayCam.enabled = false;
        }
    }
}
