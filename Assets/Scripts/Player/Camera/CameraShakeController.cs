using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShakeController : MonoBehaviour
{
    public static CameraShakeController Instance;

    CinemachineVirtualCamera vCam;

    CinemachineBasicMultiChannelPerlin perline;

    private void Awake()
    {
        Instance = this;

        vCam = GetComponent<CinemachineVirtualCamera>();

        perline = vCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        perline.m_AmplitudeGain = 0f;
    }

    public void ShakeCamera(float intensity, float time)
    {
        StopAllCoroutines();
        perline.m_AmplitudeGain = intensity;
        StartCoroutine(ShakeTime(intensity,time));
    }

    IEnumerator ShakeTime(float intensity, float time)
    {
        yield return new WaitForSeconds(time);
        perline.m_AmplitudeGain = Mathf.Lerp(intensity, 0f, 1f);
    }
}
