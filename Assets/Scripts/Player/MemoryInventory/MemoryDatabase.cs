using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Database : MonoBehaviour, IDataPersistence
{
    [Header("Non guardare la lista nell'inspector mentre esci dal play.")]
    [Header("Se lo fai la lista esplode.")]
    [Header("Per risolvere basta avere un'altro Gameobject nell'inspector.")]
    [Header("Entrare ed uscire dal play.")]

    public SerializableDictionary<string,BaseNoteMemory> noteDictionary = new SerializableDictionary<string, BaseNoteMemory>();
    public SerializableDictionary<string, BaseDialogueMemory> dialogueDictionary = new SerializableDictionary<string, BaseDialogueMemory>();

    public List<NoteInteraction> noteInteractions = new List<NoteInteraction>();
    public List<GameObject> dialogueInteractions = new List<GameObject>();

    public List<PuzzleIsSolved> puzzlesSolved = new List<PuzzleIsSolved>();

    public TastierinoManager tastierinoManager;

    public void SaveData(GameData data)
    {
        data.tastierinoSolution = tastierinoManager.solution;
        data.noteMemory = noteDictionary;
        data.dialogMemory = dialogueDictionary;
        data.noteInteractions = noteInteractions;
        data.dialogueInteractions = dialogueInteractions;
        data.puzzlesSolved = puzzlesSolved;
    }
    public void LoadData(GameData data)
    {
        tastierinoManager.solution = data.tastierinoSolution;
        noteDictionary = data.noteMemory;
        dialogueDictionary = data.dialogMemory;
        noteInteractions = data.noteInteractions;
        dialogueInteractions = data.dialogueInteractions;
        puzzlesSolved = data.puzzlesSolved;

        foreach (NoteInteraction interaction in noteInteractions)
        {
            interaction.gameObject.SetActive(false);
        }
        foreach (GameObject interaction in dialogueInteractions)
        {
            interaction.gameObject.SetActive(false);
        }
        foreach (var note in noteDictionary)
        {
            note.Value.gameObject.SetActive(true);
        }
        foreach (var dialogue in dialogueDictionary)
        {
            dialogue.Value.gameObject.SetActive(true);
        }

        foreach (var puzl in puzzlesSolved)
        {
            puzl.SolvePuzzle();
        }
    }
}

