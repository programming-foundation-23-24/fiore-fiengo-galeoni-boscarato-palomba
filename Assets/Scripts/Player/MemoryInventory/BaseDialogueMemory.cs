using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BaseDialogueMemory : MonoBehaviour
{
    [HideInInspector]
    public int boolIsActive = 0;

    Database memoryDatabase;

    public GameObject poster;

    TextMeshProUGUI buttonText;
    [SerializeField] TextMeshProUGUI textDisplay;

    public string dialogueName;

    [TextArea (30,10)]
    public string dialogueContent;

    private void Awake()
    {
        buttonText = GetComponentInChildren<TextMeshProUGUI>();

        buttonText.text = dialogueName;

        memoryDatabase = GetComponentInParent<Database>();
    }

    public void OnDialogueClick()
    {
        textDisplay.text = dialogueContent;

        poster.SetActive(false);
    }
}
