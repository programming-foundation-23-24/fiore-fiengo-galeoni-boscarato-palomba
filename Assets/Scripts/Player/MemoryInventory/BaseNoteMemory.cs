using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BaseNoteMemory : MonoBehaviour
{
    Database memoryDatabase;

    TextMeshProUGUI nameText;
    [SerializeField] TextMeshProUGUI textDisplay;

    public string noteName;

    public Sprite myPoster;

    [Header("Risoluzione dell'immagine (solo una attiva pls)")]
    public bool orizontalRateo;
    public bool verticalRateo;
    public bool squareRateo;

    public GameObject posterOBJ;
    public Image memoryPoster;
    public RectTransform posterTransform;

    [TextArea (30,10)]
    public string noteContent;

    private void Awake()
    {
        nameText = GetComponentInChildren<TextMeshProUGUI>();

        nameText.text = noteName;

        memoryDatabase = GetComponentInParent<Database>();
    }

    public void OnNoteClick()
    {
        textDisplay.text = noteContent;

        if(myPoster != null)
        {
            posterOBJ.SetActive(true);

            memoryPoster.color = Color.white;
            memoryPoster.sprite = myPoster;

            if (orizontalRateo)
            {
                posterTransform.sizeDelta = new Vector2(768, 432);
            }
            else if (verticalRateo)
            {
                posterTransform.sizeDelta = new Vector2(432, 768);
            }
            else if (squareRateo)
            {
                posterTransform.sizeDelta = new Vector2(600, 600);
            }
        }
        else
        {
            memoryPoster.sprite = null;
            memoryPoster.color = Color.clear;
        }
    }
}
