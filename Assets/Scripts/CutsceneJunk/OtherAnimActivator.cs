using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherAnimActivator : MonoBehaviour
{
    public Animator otherAnimator;

    public void StartWalk()
    {
        otherAnimator.SetTrigger("Walk");
    }    
    public void StartIdle()
    {
        otherAnimator.SetTrigger("Idle");
    }
}
