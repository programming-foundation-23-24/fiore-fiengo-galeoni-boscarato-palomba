using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroGameStarter : MonoBehaviour
{
    private void Start()
    {
        if (DataPersistenceManager.instance.haveSaved == 1)
        {
            gameObject.SetActive(false);
        }
    }
    public void ShowPlayer()
    {
        GameManager.Instance.ShowPlayer();
    }
}
