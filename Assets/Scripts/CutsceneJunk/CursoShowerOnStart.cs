using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursoShowerOnStart : MonoBehaviour
{
    void Start()
    {
        GameManager.Instance.ShowCursor();
    }
}
