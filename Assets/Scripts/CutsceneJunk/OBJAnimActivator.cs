using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OBJAnimActivator : MonoBehaviour
{
    public List<GameObject> dialogueTrigger = new List<GameObject>();
    public List<GameObject> genericOBJ = new List<GameObject>();

    public void ActivateOBJ()
    {
        foreach (GameObject obj in dialogueTrigger)
        {
            obj.SetActive(true);
        }
    }    
    public void ActivateOBJALT()
    {
        foreach (GameObject obj in genericOBJ)
        {
            obj.SetActive(true);
        }
    }
}
