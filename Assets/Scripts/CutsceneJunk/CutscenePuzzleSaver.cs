using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutscenePuzzleSaver : MonoBehaviour
{
    public Animator animator;

    public BoxCollider mycollider;

    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
        
    }

    public void EnableCollider()
    {
        StartCoroutine(WaitToCollide());
    }

    public void ResetTrigger()
    {
        animator.SetTrigger("Idle");
    }

    IEnumerator WaitToCollide()
    {
        yield return new WaitForFixedUpdate();
        mycollider.enabled = true;
    }
}
