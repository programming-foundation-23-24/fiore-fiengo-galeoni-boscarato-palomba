using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;


public class VolumeSettings : MonoBehaviour
{
    public Slider musicSlider;
    public Slider SFXSlider;
    public Slider masterSlider;

    [SerializeField] AudioMixer myMixer;

    public VolumeSettings otherVolume;
   

    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.HasKey("musicVolume"))
        {
            loadVolume();
        }
        else
        {
            setMusicVolume();
        }
        
        if (PlayerPrefs.HasKey("SFXVolume"))
        {
            loadSFXVolume();
        }
        else
        {
            setSFXVolume();
        }

        if (PlayerPrefs.HasKey("masterVolume"))
        {
            loadMasterVolume();
        }
        else
        {
            setMasterVolume();
        }


    }

    public void setMusicVolume()
    {
        float volume = musicSlider.value;
        myMixer.SetFloat("music", Mathf.Log10(volume) * 20 );
        PlayerPrefs.SetFloat("musicVolume", volume);

        Equalizer();
    }

    void loadVolume()
    {
        musicSlider.value = PlayerPrefs.GetFloat("musicVolume");
        setMusicVolume();

        Equalizer();
    }

    
    public void setSFXVolume()
    {
        float SFXVolume = SFXSlider.value;
        myMixer.SetFloat("soundFX", Mathf.Log10(SFXVolume) * 20);
        PlayerPrefs.SetFloat("SFXVolume", SFXVolume);

        Equalizer();
    }

    void loadSFXVolume()
    {
        SFXSlider.value = PlayerPrefs.GetFloat("SFXVolume");
        setSFXVolume();
    }


    public void setMasterVolume()
    {
        float masterVolume = masterSlider.value;
        myMixer.SetFloat("master", Mathf.Log10(masterVolume) * 20);
        PlayerPrefs.SetFloat("masterVolume", masterVolume);
    }

    void loadMasterVolume()
    {
        masterSlider.value = PlayerPrefs.GetFloat("masterVolume");
        setMasterVolume();
    }

    void Equalizer()
    {
        otherVolume.masterSlider.value = masterSlider.value;
        otherVolume.musicSlider.value = musicSlider.value;
        otherVolume.SFXSlider.value = SFXSlider.value;
    }


}
