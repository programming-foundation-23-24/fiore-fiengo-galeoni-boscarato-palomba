using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonCursorChanger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    Texture2D textureCursore;
    private CursorMode cursorMode = CursorMode.Auto;
    private Vector2 hotSpot = Vector2.zero;

    [HideInInspector]
    public GameManager gm;

    void Start()
    {
        gm = FindAnyObjectByType<GameManager>().GetComponent<GameManager>();
        textureCursore = gm.customCursor;

        if (textureCursore == null)
        {
            Debug.LogError("Texture del cursore non impostata!");
            enabled = false;
            return;
        } 
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Cursor.SetCursor(textureCursore, hotSpot, cursorMode);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }
}
