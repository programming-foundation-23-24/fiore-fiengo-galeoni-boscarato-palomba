using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class RespawnButton : MonoBehaviour
{
    public void Respawn()
    {
        DataPersistenceManager.instance.LoadGame();

        GameManager.Instance.DeathMenuToggle();
        GameManager.Instance.HideAllPrompts();

        GameManager.Instance.ResetPlayerAfterRespawn();
    }
}
