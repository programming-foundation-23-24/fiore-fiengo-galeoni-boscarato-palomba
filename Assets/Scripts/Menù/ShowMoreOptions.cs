using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMoreOptions : MonoBehaviour
{
    public bool isOptionsShown;

    public GameObject toShowMenu;

    public void Click()
    {
        if (!isOptionsShown)
        {
            toShowMenu.SetActive(true);

            isOptionsShown = true;
        }
        else
        {
            toShowMenu.SetActive(false);

            isOptionsShown = false;
        }
    }
}
