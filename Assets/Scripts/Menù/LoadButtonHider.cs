using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadButtonHider : MonoBehaviour
{
    public Button myButton;

    private void Update()
    {
        if (HaveSaved() == 1)
        {
            myButton.interactable = true;
        }
        else
        {
            myButton.interactable = false;
        }
    }
    int HaveSaved()
    {
        return PlayerPrefs.GetInt("HaveSaved");
    }

    public void Load()
    {
        SceneManager.LoadSceneAsync(1);
        DataPersistenceManager.instance.LoadGame();
    }
}