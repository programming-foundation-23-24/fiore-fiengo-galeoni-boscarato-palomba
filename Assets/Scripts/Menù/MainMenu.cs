using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    [Header("Menu Buttons")]
    [SerializeField] private Button newGameButton;
    [SerializeField] private Button continueGameButton;
    public SoundPlayerTrigger source;

    private void Awake()
    {
        Cursor.visible = true;
    }
    private void Start()
    {
        if (DataPersistenceManager.instance.haveSaved == 0)
        {
            continueGameButton.interactable = false;
        }
        Time.timeScale = 1;
    }

    public void OnNewGameClicked()
    {
        DataPersistenceManager.instance.NewGame();
    }

    public void OnContinueGameClicked()
    {
        DataPersistenceManager.instance.LoadGame();
        StartCoroutine(DelaySecondLoadLoad());
    }
    IEnumerator DelaySecondLoadLoad()
    {
        yield return new WaitForEndOfFrame();
        DataPersistenceManager.instance.LoadGame();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}