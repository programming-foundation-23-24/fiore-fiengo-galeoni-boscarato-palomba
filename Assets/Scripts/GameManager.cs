using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour//, IDataPersistence
{
    public UnityEvent OnNewGame, OnContinue;

    public static GameManager Instance;

    [SerializeField] GameObject deathMenu;
    public GameObject Player;
    public GameObject PlayerModel;
    public GameObject defaultPrompt;
    public GameObject memoryMenu;
    public GameObject saveFeedback;
    GameObject cachedCustomPrompt;

    PlayerAimShoot aimScript;

    PlayerMovement moveScript;

    [SerializeField]PlayerHealth healthScript;

    DimensionChange dimensionChange;

    public bool gameStarted = false;
    bool gamePaused;
    bool memoryOpen;
    bool deathMenuOpen;
    public bool RESETSAVE;
    public bool isPuzzleOnScreen;

    [Header("Pause & Menu Managing")]
    [SerializeField] GameObject pauseMenu;
    [SerializeField] GameObject settingsPauseMenu;
    [SerializeField] GameObject audioPauseMenu;
    [SerializeField] GameObject controlsPauseMenu;
    [SerializeField] GameObject sureQuitPauseMenu;


    public Database memoryDatabase;


    [Header("Puzzles Compleated")]
    public int futureKnobsAndCables = 0;

    [Header("Cursor")]
    public Texture2D customCursor;

    private void Awake()
    {
        InstanceManager();
        GetAllRef();
    }

    private void Start()
    {
        if (DataPersistenceManager.instance.haveSaved == 0)
        {
            HidePlayer();
        }
        else
        {
            gameStarted = true;
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("Pause") && gameStarted)
        {
            if (!isPuzzleOnScreen)
            {
                if (memoryOpen)
                {
                    MemoryMenu();
                }
                else
                {
                    PauseGame();
                }
            }
        }

        if (Input.GetButtonDown("MemoryMenu") && !isPuzzleOnScreen && gameStarted)
        {
            if (!gamePaused)
            {
                MemoryMenu();
            }
        }
    }

    #region INSTANCE

    private void InstanceManager()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion
    #region Get References
    private void GetAllRef()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        aimScript = Player.GetComponent<PlayerAimShoot>();
        moveScript = Player.GetComponent<PlayerMovement>();
        healthScript = Player.GetComponent<PlayerHealth>();
        dimensionChange = Player.GetComponent<DimensionChange>();
    }

    #endregion
    #region Start Game
   /* public void NewGame()
    {
        HideCursor();

        OnNewGame?.Invoke();
    }*/
    public void GameStarted()
    {
        gameStarted = true;
    }

    public void ShowPlayer()
    {
        PlayerModel.SetActive(true);
    }    
    public void HidePlayer()
    {
        PlayerModel.SetActive(false);
    }

    /*public void ContinueGame()
    {
        gameStarted = true;

        StartPlayer();

        HideCursor();

        OnContinue?.Invoke();
    }*/
    #endregion
    #region Player
    public void StopPlayer()
    {
        aimScript.scriptIsActive = false;
        moveScript.scriptIsActive = false;
        dimensionChange.scriptIsActive = false;
    }
    public void StartPlayer()
    {
        aimScript.scriptIsActive = true;
        moveScript.scriptIsActive = true;
        dimensionChange.scriptIsActive = true;
    }
    public void ResetPlayerAfterRespawn()
    {
        CancelInvoke("FreezeTime");

        healthScript.currentHealth = healthScript.maxHealth;
        healthScript.ParticlesUpdate();
        healthScript.playerMesh.SetActive(true);
    }
    public void PlayerCanShoot()
    {
        aimScript.shootingUnlocked = true;
    }

    public void DimentionOBJChange(GameObject present, GameObject future)
    {
        dimensionChange.present = present;
        dimensionChange.future = future;
    }
    #endregion
    #region Show & Hide Prompt
    public void ShowDefaultPrompt()
    {
        defaultPrompt.SetActive(true);
    }
    public void HideDefaultPrompt()
    {
        defaultPrompt.SetActive(false);
    }

    public void ShowCustomPrompt(GameObject prompt)
    {
        cachedCustomPrompt = prompt;
        prompt.SetActive(true);
    }
    public void HideCustomPrompt(GameObject prompt)
    {
        cachedCustomPrompt = null;

        if(prompt != null)
        {
            prompt.SetActive(false);
        }
    }

    public void HideAllPrompts()
    {
        HideDefaultPrompt();
        HideCustomPrompt(cachedCustomPrompt);
    }
    #endregion
    #region Check 4 Puzzle on Screen
    public void PuzzleIsOnScreen()
    {
        isPuzzleOnScreen = true;
        ShowCursor();
    }
    public void PuzzleIsNotOnScreen()
    {
        isPuzzleOnScreen = false;
        HideCursor();
    }
    #endregion
    #region Menu Managing & GamePause

    public void PauseGame()
    {
        if (!gamePaused)
        {
            pauseMenu.SetActive(true);

            FreezeTime();

            gamePaused = true;

            ShowCursor();
        }
        else
        {
            DeactivateAllPauseMenus();

            ResumeTime();

            gamePaused = false;

            HideCursor();
        }
    }
    void DeactivateAllPauseMenus()
    {
        pauseMenu.SetActive(false);
        settingsPauseMenu.SetActive(false);
        audioPauseMenu.SetActive(false);
        controlsPauseMenu.SetActive(false);
        sureQuitPauseMenu.SetActive(false);
    }

    public void MemoryMenu()
    {
        if (!memoryOpen)
        {
            memoryMenu.SetActive(true);

            FreezeTime();

            memoryOpen = true;

            ShowCursor();
        }
        else
        {
            memoryMenu.SetActive(false);

            ResumeTime();

            memoryOpen = false;

            HideCursor();
        }
    }

    public void DeathMenuToggle()
    {
        if (!deathMenuOpen)
        {
            deathMenu.SetActive(true);
            deathMenuOpen = true;

            Invoke("FreezeTime", 0.5f);

            ShowCursor();
        }
        else
        {
            deathMenu.SetActive(false);
            deathMenuOpen = false;

            ResumeTime();

            HideCursor();
        }
    }

    public void ShowCursor()
    {
        Cursor.visible = true;
    }
    public void HideCursor()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        Cursor.visible = false;
    }

    public void FreezeTime()
    {
        StopPlayer();

        Time.timeScale = 0;
    }
    public void ResumeTime()
    {
        StartPlayer();

        Time.timeScale = 1;
    }
    #endregion
    #region From Menu Commands
    public void QuitGame()
    {
        Application.Quit();
    }
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
    #endregion
    #region Note && Dialogues Logic
    public void NoteCollected(string noteName, BaseNoteMemory myNoteOnMemory, NoteInteraction interaction)
    {
        memoryDatabase.noteDictionary.Add(noteName, myNoteOnMemory);

        memoryDatabase.noteDictionary[noteName].gameObject.SetActive(true);

        memoryDatabase.noteInteractions.Add(interaction);
    }
    public void DialogueCollected(string dialogueName, BaseDialogueMemory myDialogueOnMemory, GameObject interaction) 
    {
        memoryDatabase.dialogueDictionary.Add(dialogueName, myDialogueOnMemory);

        memoryDatabase.dialogueDictionary[dialogueName].gameObject.SetActive(true);

        memoryDatabase.dialogueInteractions.Add(interaction);
    }
    #endregion
    #region Save
    public void ShowAndHideFeedback()
    {
        StartCoroutine(ShowAndHideFeedbackCoroutine());
    }
    IEnumerator ShowAndHideFeedbackCoroutine()
    {
        saveFeedback.SetActive(true);
        yield return new WaitForSeconds(2.5f);
        saveFeedback.SetActive(false);
    }
    #endregion
    #region Save Puzzles Solved

    public void SavePuzzleSolved(PuzzleIsSolved pzlslvd)
    {
        memoryDatabase.puzzlesSolved.Add(pzlslvd);
    }
    #endregion
}
