using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{
    Animator myAnimator;

    bool canWave = true;

    private void Start()
    {
        myAnimator = GetComponentInParent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && canWave)
        {
            canWave = false;

            StartCoroutine(WaveAgain());
        }
    }

    IEnumerator WaveAgain()
    {
        myAnimator.SetTrigger("Wave");

        yield return new WaitForSeconds(10);

        canWave = true;
    }
}
