using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalPiece : BasePiece
{
    public Image goalImage;

    public Color inactiveColor;
    public Color activeColor;

    private void Start()
    {
        SetConductsColor(inactiveColor);
    }

    protected override void OnRotationStarted()
    {
        goalImage.color = Color.gray;
        SetConductsColor(inactiveColor);
    }

    protected override void OnRotationEnded()
    {
        
    }

    protected override void OnActivation()
    {
        SetConductsColor(activeColor);
        goalImage.color = Color.yellow;
        puzzle.GoalHasBeenActivated(this);
    }
}