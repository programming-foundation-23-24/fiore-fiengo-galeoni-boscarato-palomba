using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RotatingPuzzle : PuzzleSolution
{
    public UnityEvent OnPuzzleCompleted;

    public float activationFlowDelay = 0.1f;
    public float rotateDuration = 0.5f;
    public List<GoalPiece> goalsToActivate = new List<GoalPiece>();
    public GameObject WIN_Anim;
    
    [Header("Runtime only")]
    public bool isRotating;
    public List<GoalPiece> remainingGoalsToActivate = new List<GoalPiece>();
    
    public event Action OnRotationStarted;
    public event Action OnRotationEnded;

    bool puzzleSolved = false;

    public GameObject myExitButton;

    public void RequestGearsRotation(List<RotatingGear> gearsToRotate)
    {
        if (isRotating)
        {
            return;
        }
        
        isRotating = true;
        OnRotationStarted?.Invoke();
        RestoreGoalToActivate();

        foreach (RotatingGear gear in gearsToRotate)
        {
            gear.StartRotation(rotateDuration);
        }
        
        StartCoroutine(StartRotationCooldown());
    }

    IEnumerator StartRotationCooldown()
    {        
        yield return new WaitForSeconds(rotateDuration);
        isRotating = false;
        OnRotationEnded?.Invoke();
    }

    void RestoreGoalToActivate()
    {
        remainingGoalsToActivate.Clear();
        remainingGoalsToActivate.AddRange(goalsToActivate);
    }

    public void GoalHasBeenActivated(GoalPiece activatedGoal)
    {
        if (remainingGoalsToActivate.Contains(activatedGoal))
        {
            remainingGoalsToActivate.Remove(activatedGoal);

            if (remainingGoalsToActivate.Count == 0)
            {
                WIN_Anim.SetActive(true);

                puzzleSolved = true;

                completionSound.Play();

                OnPuzzleCompleted?.Invoke();
            }
        }
    }

    public void SolvePuzzle()
    {
        if (puzzleSolved)
        {
            PuzzleSolved();
        }
    }

}
