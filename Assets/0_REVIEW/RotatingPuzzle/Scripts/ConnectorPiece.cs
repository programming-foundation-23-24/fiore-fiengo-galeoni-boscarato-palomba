using UnityEngine;

public class ConnectorPiece : BasePiece
{
    public Color inactiveColor;
    public Color activeColor;

    private void Start()
    {
        SetConductsColor(inactiveColor);
    }

    protected override void OnRotationStarted()
    {
        SetConductsColor(inactiveColor);
    }
    
    protected override void OnRotationEnded()
    {
        
    }

    protected override void OnActivation()
    {
        SetConductsColor(activeColor);
    }
}