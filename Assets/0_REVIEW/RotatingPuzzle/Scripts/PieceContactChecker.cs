using System;
using UnityEngine;

public class PieceContactChecker : MonoBehaviour
{
    public BasePiece piece;

    void Awake()
    {
        if (piece == null)
        {
            piece = GetComponentInParent<BasePiece>();
        }        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        PieceContactChecker otherContact = other.GetComponent<PieceContactChecker>();

        if (otherContact != null)
        {
            piece.AddConnectedPiece(otherContact.piece);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        PieceContactChecker otherContact = other.GetComponent<PieceContactChecker>();

        if (otherContact != null)
        {
            piece.RemoveConnectedPiece(otherContact.piece);
        }
    }
}