using System;
using System.Collections;
using System.Linq;
using UnityEngine;


public class RotatingGear : MonoBehaviour
{
    public float targetZRotation = 90;
    
    public void StartRotation(float rotationTime)
    {
        StartCoroutine(Rotate(rotationTime));
    }
    
    IEnumerator Rotate(float rotationTime)
    {
        float elapsedTime = 0f;
        Quaternion initialRotation = transform.rotation;
        Quaternion targetRotation = TargetRotation();

        while (elapsedTime < rotationTime)
        {
            elapsedTime += Time.deltaTime;
            float t = Mathf.Clamp01(elapsedTime / rotationTime);
            
            transform.rotation = Quaternion.Slerp(initialRotation, targetRotation, t);
            yield return null;
        }

        transform.rotation = targetRotation;
    }
    Quaternion TargetRotation()
    {
        return transform.rotation * Quaternion.Euler(0f,0f, -targetZRotation);
    }
}