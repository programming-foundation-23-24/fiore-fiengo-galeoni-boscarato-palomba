using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BasePiece : MonoBehaviour
{
    public bool isActive;
    public RotatingPuzzle puzzle;
    public List<Image> conductImages = new List<Image>();
    public List<BasePiece> connectedPieces = new List<BasePiece>();

    protected virtual void Awake()
    {
        puzzle.OnRotationStarted += ForceDeactivationOnRotationStarted;
        puzzle.OnRotationEnded += OnRotationEnded;
    }
    
    protected virtual void OnDestroy()
    {
        puzzle.OnRotationStarted -= ForceDeactivationOnRotationStarted;
        puzzle.OnRotationEnded -= OnRotationEnded;
    }

    protected abstract void OnRotationStarted();
    protected abstract void OnRotationEnded();
    protected abstract void OnActivation();

    void ForceDeactivationOnRotationStarted()
    {
        isActive = false;
        StopAllCoroutines();
        OnRotationStarted();
    }
    
    public void Activate()
    {
        if (isActive)
        {
            return;
        }

        isActive = true;
        StartCoroutine(DelayActivation());
    }

    IEnumerator DelayActivation()
    {
        yield return new WaitForSeconds(puzzle.activationFlowDelay);
        ActivateConnectedPieces();
        OnActivation();
    }
    
    public void AddConnectedPiece(BasePiece pieceToAdd)
    {
        if (!connectedPieces.Contains(pieceToAdd))
        {
            connectedPieces.Add(pieceToAdd);
        }
    }

    public void RemoveConnectedPiece(BasePiece pieceToRemove)
    {
        if (connectedPieces.Contains(pieceToRemove))
        {
            connectedPieces.Remove(pieceToRemove);
        }
    }

    void ActivateConnectedPieces()
    {
        foreach (BasePiece connectedPiece in connectedPieces)
        {
            connectedPiece.Activate();            
        }
    }
    
    protected void SetConductsColor(Color color)
    {
        foreach (Image image in conductImages)
        {
            image.color = color;
        }
    }
}