using UnityEngine;

public class StartPiece : BasePiece
{
    protected override void OnRotationStarted()
    {
        SetConductsColor(Color.cyan);
    }

    protected override void OnRotationEnded()
    {
        Activate();
    }

    protected override void OnActivation()
    {
        SetConductsColor(Color.yellow);
    }
}