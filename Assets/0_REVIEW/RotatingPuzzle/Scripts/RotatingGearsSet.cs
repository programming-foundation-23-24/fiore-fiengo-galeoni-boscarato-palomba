using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotatingGearsSet : MonoBehaviour
{
    public RotatingPuzzle puzzle;
    public Button button;
    public List<RotatingGear> gears = new List<RotatingGear>();

    void Awake()
    {
        button.onClick.AddListener(OnClick);
    }

    void OnDestroy()
    {
        button.onClick.RemoveListener(OnClick);
    }

    void OnClick()
    {
        puzzle.RequestGearsRotation(gears);
    }
}