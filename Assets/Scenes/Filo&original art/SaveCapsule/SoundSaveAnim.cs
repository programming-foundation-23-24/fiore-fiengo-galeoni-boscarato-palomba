using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSaveAnim : MonoBehaviour
{
    public AudioClip goUp;
    public AudioClip goDown;

    public AudioSource source;

    public void GoUp()
    {
        source.clip = goUp;

        source.Play();
    }
    public void GoDown()
    {
        source.clip = goDown;

        source.Play();
    }
}
