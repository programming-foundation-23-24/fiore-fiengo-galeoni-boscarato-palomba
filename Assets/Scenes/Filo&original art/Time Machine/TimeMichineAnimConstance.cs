using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeMichineAnimConstance : MonoBehaviour
{
    public bool isActive;

    public Animator animator;

    public AudioSource sourceLever;
    public AudioSource sourceLoop;

    private void Update()
    {
        if (isActive)
        {
            animator.SetTrigger("Play");
        }
    }
    public void IsActive()
    {
        isActive = true;
    }

    public void PlayLeverSound()
    {
        sourceLever.Play();
    }    
    public void PlayLoopSound()
    {
        sourceLoop.Play();
    }
}
